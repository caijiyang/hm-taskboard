package com.felixcjy.hmtaskboard.dao;

import com.felixcjy.hmtaskboard.pojo.*;
import ohos.data.orm.OrmDatabase;
import ohos.data.orm.annotation.Database;

/**
 * 数据库
 *
 * @author 蔡济阳（FelixCJY / FelixCai）
 * @date 2022/04/08 08:22
 **/

@Database(entities = {IsLogin.class, TUser.class, TBoard.class, Pages.class, Card.class, Label.class}, version = 1)
public abstract class Htk extends OrmDatabase {
}