package com.felixcjy.hmtaskboard.slice;

import com.felixcjy.hmtaskboard.ResourceTable;
import com.felixcjy.hmtaskboard.TaskBoardAbility;
import com.felixcjy.hmtaskboard.pojo.TBoard;
import com.felixcjy.hmtaskboard.pojo.TUser;
import com.felixcjy.hmtaskboard.provider.TBoardProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.colors.RgbColor;
import ohos.agp.colors.RgbPalette;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.data.DatabaseHelper;
import ohos.data.orm.OrmContext;
import ohos.data.orm.OrmPredicates;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;
import java.util.List;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_CONTENT;

/**
 * 主页面
 */

public class HomePageAbilitySlice extends AbilitySlice {

    private static final HiLogLabel hilog_label = new HiLogLabel(HiLog.LOG_APP, 0, "hmtb_HomePageAbilitySlice");
    private static final String hilog_format = "%{public}s";

    ListContainer listContainer_taskBoard = null;
    private List<TBoard> listTBoardItem = new ArrayList<TBoard>();
    private TBoardProvider tBoardProvider = null;
    Integer add_color = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_home);
        // 初始化界面
        initListContainer();
        // 跳转 设置详情 页面
        Image detail = (Image) findComponentById(ResourceTable.Id_home_detail);
        detail.setClickedListener(component -> {
            Intent toSetUp = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withAction("action.homeToSetup")
                    .build();
            toSetUp.setOperation(operation);
//            present(new SetupAbilitySlice(), toSetUp);
            startAbility(toSetUp);
        });

        // 添加看板 按钮
        Image add = (Image) findComponentById(ResourceTable.Id_home_add);
        add.setClickedListener(component -> {
            // 设置对话框
            CommonDialog dialog = new CommonDialog(getContext());
            // 获取 对话框的样式资源
            Component content_container = LayoutScatter.getInstance(getContext())
                    .parse(ResourceTable.Layout_add_layout_custom_dialog_content,
                            null, false);
            // 设置自定义内容
            dialog.setContentCustomComponent(content_container);
            dialog.setSize(MATCH_CONTENT, MATCH_CONTENT);
            // 设置色卡状态
            RadioContainer radioContainer = (RadioContainer)
                    content_container.findComponentById(ResourceTable.Id_add_input_color_select);
            int count = radioContainer.getChildCount();
            for (int i = 0; i < count; i++) {
                ((RadioButton) radioContainer.getComponentAt(i)).setButtonElement(createStateElement(i));
            }
            // 确认添加 按钮的监听
            Button button = (Button) content_container.findComponentById(ResourceTable.Id_add_input_btn);
            TextField add_board_title = (TextField) content_container.findComponentById(ResourceTable.Id_add_input_title);
            TextField add_board_describe = (TextField)
                    content_container.findComponentById(ResourceTable.Id_add_input_content);
            radioContainer.setMarkChangedListener((radioContainer1, index) -> {
                add_color = index;
            });
            button.setClickedListener(component1 -> {
                addTestData(add_color, add_board_title.getText(),add_board_describe.getText());
                add_color = 0;
                copyItem(getDataFromDB());
                cleanText();
                tBoardProvider.notifyDataChanged();
                dialog.destroy();
            });
            dialog.setButton(IDialog.BUTTON1, "取消", (iDialog, i) -> iDialog.destroy());
            dialog.show();
        });

        Image search = findComponentById(ResourceTable.Id_home_search);
        search.setClickedListener(component -> {
            Intent search_intent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withAction("action.home.search")
                    .build();
            search_intent.setOperation(operation);
            startAbility(search_intent);
        });
    }

    // 将内容加入到 listContainer 中，但保证数据源一致。
    private void copyItem(List<TBoard> dataFromDB) {
        listTBoardItem.clear();
        listTBoardItem.addAll(dataFromDB);
    }

    // 设置 色卡的状态
    private StateElement createStateElement(int i) {
        RgbColor rgbColor = null;
        if (i == 0) {
            rgbColor = new RgbColor(207,197,226);
        } else if (i == 1) {
            rgbColor = new RgbColor(255,184,179);
        } else if (i == 2) {
            rgbColor =new RgbColor(181,204,141);
        } else if (i == 3) {
            rgbColor = new RgbColor(166,186,221);
        } else {
            rgbColor = new RgbColor(241,215,159);
        }

        // 按下的状态
        ShapeElement elementButtonOn = new ShapeElement();
        elementButtonOn.setRgbColor(rgbColor);
        elementButtonOn.setStroke(8, RgbPalette.WHITE);
        elementButtonOn.setShape(ShapeElement.RECTANGLE);

        // 正常状态
        ShapeElement elementButtonOff = new ShapeElement();
        elementButtonOff.setRgbColor(rgbColor);
        elementButtonOff.setShape(ShapeElement.RECTANGLE);
        elementButtonOff.setCornerRadius(8.0f);

        // 状态绑定
        StateElement checkElement = new StateElement();
        checkElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, elementButtonOn);
        checkElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, elementButtonOff);
        return checkElement;
    }

    // 初始化 listContainer
    private void initListContainer() {

        listContainer_taskBoard = (ListContainer) findComponentById(ResourceTable.Id_home_listContainer);
        copyItem(getDataFromDB());
        tBoardProvider = new TBoardProvider(listTBoardItem, this);
        listContainer_taskBoard.setItemProvider(tBoardProvider);
        cleanText();
        // 看板列表的点击监听事件
        listContainer_taskBoard.setItemClickedListener((container, component, position, id) -> {
            TBoard taskBoard = (TBoard) listContainer_taskBoard.getItemProvider().getItem(position);
            Intent intent_board = new Intent();
            Operation operation = new Intent.OperationBuilder().withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName(TaskBoardAbility.class.getName())
                    .build();
            intent_board.setOperation(operation);
            // 目前：值转成 String 才能传过去。
            intent_board.setParam("tbId", taskBoard.getId() + "");
            startAbility(intent_board);
        });
        // 关闭长按响应，因为长按会有拖动效果，但拖动无法改变排序。
        listContainer_taskBoard.setLongClickable(false);
    }

    // 从数据库中拿取数据。
    private List<TBoard> getDataFromDB() {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext getOrmContext_htk = helper.getOrmContext("Htk");
        // 查询登录的人是否存在
        OrmPredicates query_user = getOrmContext_htk.where(TUser.class).equalTo("state",1);
        TUser tUser = (TUser) getOrmContext_htk.query(query_user).get(0);
        OrmPredicates query = getOrmContext_htk.where(TBoard.class).equalTo("userId",tUser.getId());
        return getOrmContext_htk.query(query);
    }

    // 添加看板到数据库。
    private void addTestData(Integer color, String title, String describe) {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext getOrmContext_htk = helper.getOrmContext("Htk");
        OrmPredicates query_user = getOrmContext_htk.where(TUser.class).equalTo("state",1);
        TUser tUser = (TUser) getOrmContext_htk.query(query_user).get(0);
        TBoard tBoard = new TBoard(color,title,describe,tUser.getId());
        getOrmContext_htk.insert(tBoard);
        getOrmContext_htk.flush();
    }

    // 清理提示文字
    private void cleanText() {
        if (listTBoardItem.size() != 0) {
            Text homeNullDataText = (Text) findComponentById(ResourceTable.Id_home_null_data_text);
            homeNullDataText.setText("");
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
        // 切回来的时候更新数据，这样就能看见被修改的数据了。
        copyItem(getDataFromDB());
        cleanText();
        tBoardProvider.notifyDataChanged();
    }
}
