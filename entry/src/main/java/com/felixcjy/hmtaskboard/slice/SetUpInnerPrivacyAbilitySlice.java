package com.felixcjy.hmtaskboard.slice;

import com.felixcjy.hmtaskboard.ResourceTable;
import com.felixcjy.hmtaskboard.pojo.PrivatePolicy;
import com.felixcjy.hmtaskboard.utils.JsonUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.utils.zson.ZSONObject;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_CONTENT;

public class SetUpInnerPrivacyAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_set_up_inner_privacy);
        initSetUpInnerPrivacyAbilitySlice();
    }

    private void initSetUpInnerPrivacyAbilitySlice() {
        Image image = (Image) findComponentById(ResourceTable.Id_privacy_back);
        image.setClickedListener(component -> {
            onBackPressed();
        });

        Text privacy = (Text) findComponentById(ResourceTable.Id_privacy_privacy_text);
        privacy.setClickedListener(component -> {
            String json = JsonUtils.getJsonFromPath("entry/resources/rawfile/privatePolicy.json", this);
            PrivatePolicy privatePolicy = ZSONObject.stringToClass(json, PrivatePolicy.class);
            CommonDialog dialog = new CommonDialog(getContext());
            Component container = LayoutScatter.getInstance(getContext())
                    .parse(ResourceTable.Layout_private_dialog, null, false);
            Text text = container.findComponentById(ResourceTable.Id_private_dialog_text);
            text.setText(privatePolicy.getPrivate_policy());
            dialog.setContentCustomComponent(container);
            dialog.setSize(MATCH_CONTENT, MATCH_CONTENT);
            dialog.setButton(IDialog.BUTTON3, "已知晓", (iDialog, i) -> iDialog.destroy());
            dialog.show();
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}