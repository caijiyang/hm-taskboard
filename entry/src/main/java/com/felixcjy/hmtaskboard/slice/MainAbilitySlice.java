package com.felixcjy.hmtaskboard.slice;

import com.felixcjy.hmtaskboard.HomePageAbility;
import com.felixcjy.hmtaskboard.ResourceTable;
import com.felixcjy.hmtaskboard.dao.Htk;
import com.felixcjy.hmtaskboard.pojo.IsLogin;
import com.felixcjy.hmtaskboard.utils.NotificationUtils;
import com.felixcjy.hmtaskboard.utils.PermissionBridge;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.data.DatabaseHelper;
import ohos.data.orm.OrmContext;
import ohos.data.orm.OrmPredicates;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.List;

/**
 * 登录之后用于判断的跳转到哪一个页面的类。
 */

public class MainAbilitySlice extends AbilitySlice implements PermissionBridge.OnPermissionStateListener {

    private static final HiLogLabel hiloglabel = new HiLogLabel(HiLog.LOG_APP, 0, "hmtb_MainAbilitySlice");
    private static final String log_format = "%{public}s";
    IsLogin isLogin_state;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        // 开启通知
        NotificationUtils.initNotificationUtils();
        // 数据库操作
        operationDB();
        // 判断用户登录状态
        if (isLogin_state.getState() == 1) {
            // 已登录，跳转主页面，包名是：com.felixcjy.hmtaskbord
            Intent main_intent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName(HomePageAbility.class.getName())
                    .build();
            main_intent.setOperation(operation);
            startAbility(main_intent);
            this.terminate();
        } else if (isLogin_state.getState() == 0) {
            // 未登录，跳转到 登录页面
            Intent login = new Intent();
            present(new LoginAbilitySlice(), login);
            this.terminate();
        }

        // 权限问题
        //  new PermissionBridge().setOnPermissionStateListener(this);
        super.setUIContent(ResourceTable.Layout_ability_home);
    }

    /**
     * 数据库操作，软件初始化数据库操作。
     */
    public void operationDB() {
        // 如果数据库不存在，创建数据库
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext ormContext = helper.getOrmContext("Htk", "Htk.db", Htk.class);
        HiLog.info(hiloglabel, log_format, "创建：数据库位置：" + this.getDatabaseDir());
        // 查询数据库
        OrmPredicates query = ormContext.where(IsLogin.class);
        List<IsLogin> dBisLogins = ormContext.query(query);
        if (dBisLogins.size() == 0) {
            // 数据表为空，插入数据
            IsLogin isLogin = new IsLogin();
            isLogin.setId(1);
            isLogin.setState(0);
            ormContext.insert(isLogin);
            ormContext.flush();
            isLogin_state = isLogin;
        } else {
            // 数据库不为空，则调取数据。
            isLogin_state = dBisLogins.get(0);
        }
    }

    @Override
    public void onPermissionGranted() {
        //这里的代码一定是在授权后才执行
    }

    @Override
    public void onPermissionDenied() {
        //这里的代码用户禁止敏感权限后被调用
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
