package com.felixcjy.hmtaskboard.slice;

import com.felixcjy.hmtaskboard.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Image;

public class SetUpInnerAboutAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_set_up_inner_about);
        initSetUpInnerAboutAbilitySlice();
    }

    private void initSetUpInnerAboutAbilitySlice() {
        Image image = (Image) findComponentById(ResourceTable.Id_about_back);
        image.setClickedListener(component -> {
            onBackPressed();
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
