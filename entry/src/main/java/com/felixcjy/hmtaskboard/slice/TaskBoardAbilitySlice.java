package com.felixcjy.hmtaskboard.slice;

import com.felixcjy.hmtaskboard.CardDetailAbility;
import com.felixcjy.hmtaskboard.ResourceTable;
import com.felixcjy.hmtaskboard.pojo.Card;
import com.felixcjy.hmtaskboard.pojo.Pages;
import com.felixcjy.hmtaskboard.pojo.TBoard;
import com.felixcjy.hmtaskboard.provider.CardProvider;
import com.felixcjy.hmtaskboard.provider.TaskBoardPageProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.colors.RgbColor;
import ohos.agp.colors.RgbPalette;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.service.WindowManager;
import ohos.data.DatabaseHelper;
import ohos.data.orm.OrmContext;
import ohos.data.orm.OrmPredicates;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;
import java.util.List;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_CONTENT;

public class TaskBoardAbilitySlice extends AbilitySlice {

    private static final HiLogLabel HiLog_Label = new HiLogLabel(HiLog.LOG_APP, 0, "hmtb_TaskBoardAbilitySlice");
    private static final String HiLog_Format = "%{public}s";

    private String tBoardId = null;
    private TBoard tBoard = null;
    TaskBoardPageProvider taskBoardPageProvider;
    PageSlider pageSlider;

    List<Pages> pages;
    List<List<Card>> listCards;
    List<TaskBoardPageProvider.TbPage> page_list;
    Integer change_color = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_task_board);
        tBoardId = intent.getStringParam("tbId");
        // 获取所属看板信息
        tBoard = getTBoard();
        // 初始化界面
        initTaskBoardAbilitySlice();
        // 初始化页面按钮
        initButton();
    }

    // 初始化页面按钮
    public void initButton() {
        for (int i = 0; i < taskBoardPageProvider.listButton.size(); i++) {
            int pageIndex = i;
            // 添加卡片
            taskBoardPageProvider.listButton.get(i).setClickedListener(component -> {
                addCardDialog(pages.get(pageIndex).getId());
            });
            // 修改页面信息
            taskBoardPageProvider.listImg.get(i).setClickedListener(component -> {
                changePage(pages.get(pageIndex).getId());
            });
        }
    }

    // 修改页面信息
    private void changePage(int pageId) {
        CommonDialog dialog = new CommonDialog(this);
        Component content_container = LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_change_page,
                        null, false);
        TextField textField = content_container.findComponentById(ResourceTable.Id_change_page_input);
        textField.setHint(getPageInfo(pageId, "title"));
        Button button_confirm = content_container.findComponentById(ResourceTable.Id_change_page_button);
        button_confirm.setClickedListener(component -> {
            if (textField.getText().equals("")) {
                // 发出警告，不可为空
                createDialog("标题不可为空");
            } else {
                changePageInfo(pageId, "title", textField.getText());
                dialog.destroy();
            }
        });
        Button button_del = content_container.findComponentById(ResourceTable.Id_change_page_button_del);
        button_del.setClickedListener(component -> {
            changePageInfo(pageId, "all", "");
            dialog.destroy();
        });
        dialog.setContentCustomComponent(content_container);
        dialog.setSize(MATCH_CONTENT, MATCH_CONTENT);
        dialog.setButton(IDialog.BUTTON1, "取消", (iDialog, i1) -> iDialog.destroy());
        dialog.show();
    }

    private void changePageInfo(int pageId, String info, String content) {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext getPageInfoOrmContext = helper.getOrmContext("Htk");
        OrmPredicates query = getPageInfoOrmContext.where(Pages.class).equalTo("id", pageId);
        List<Pages> pageList = getPageInfoOrmContext.query(query);
        Pages getPageFromList = pageList.get(0);
        switch (info) {
            case "title":
                getPageFromList.setTitle(content);
                getPageInfoOrmContext.update(getPageFromList);
                getPageInfoOrmContext.flush();
                break;
            case "all":
                // 删除页面
                OrmPredicates queryCard = getPageInfoOrmContext.where(Card.class).equalTo("pageId", pageId);
                List<Card> cardList = getPageInfoOrmContext.query(queryCard);
                HiLog.info(HiLog_Label, HiLog_Format, "几张卡？" + cardList.size());
                for (Card card : cardList) {
                    getPageInfoOrmContext.delete(card);
                    getPageInfoOrmContext.flush();
                }
                getPageInfoOrmContext.delete(getPageFromList);
                getPageInfoOrmContext.flush();
                break;
        }
        upDataAll();
    }

    private String getPageInfo(int pageId, String info) {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext getPageInfoOrmContext = helper.getOrmContext("Htk");
        OrmPredicates query = getPageInfoOrmContext.where(Pages.class).equalTo("id", pageId);
        List<Pages> pageList = getPageInfoOrmContext.query(query);
        Pages getPageFromList = pageList.get(0);
        switch (info) {
            case "title":
                return getPageFromList.getTitle();
            case "tBoardId":
                return String.valueOf(getPageFromList.gettBoardId());
        }
        return "";
    }

    // 添加卡片弹窗
    private void addCardDialog(int pageId) {
        CommonDialog dialog = new CommonDialog(this);
        Component content_container = LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_add_page_dialog,
                        null, false);
        Text layout_title = (Text) content_container.findComponentById(ResourceTable.Id_add_page_title);
        layout_title.setText("添加卡片");
        TextField layout_input = (TextField) content_container.findComponentById(ResourceTable.Id_add_page_input);
        layout_input.setHint("请输入卡片标题");
        TextField add_input_title = (TextField) content_container.findComponentById(ResourceTable.Id_add_page_input);
        Button addPageButton = (Button) content_container.findComponentById(ResourceTable.Id_add_page_button);
        addPageButton.setClickedListener(component1 -> {
            if (add_input_title.getText().equals("")) {
                dialog.destroy();
            } else {
                addCard(Integer.parseInt(tBoardId), pageId, add_input_title.getText());
                dialog.destroy();
            }
        });
        dialog.setContentCustomComponent(content_container);
        dialog.setSize(MATCH_CONTENT, MATCH_CONTENT);
        dialog.setButton(IDialog.BUTTON1, "取消", (iDialog, i1) -> iDialog.destroy());
        dialog.show();
    }

    // 添加卡片到数据库
    private void addCard(Integer taskBoardId, Integer pageId, String title) {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext addCardOrmContext = helper.getOrmContext("Htk");
        addCardOrmContext.insert(new Card(taskBoardId, pageId, title));
        addCardOrmContext.flush();
        upDataAll();
    }

    private void initTaskBoardAbilitySlice() {
        // 设置 看板标题 信息
        Text bg_title = (Text) findComponentById(ResourceTable.Id_tb_page_title_text);
        String tBTitle = tBoard.getTitle();
        if (tBTitle.equals("")) {
            bg_title.setText("（无标题）");
            bg_title.setTextColor(new Color(0xFF8C8888));
        } else {
            bg_title.setText(tBTitle);
        }
        // 返回按钮
        Image bg_back = (Image) findComponentById(ResourceTable.Id_tb_page_title_back);
        bg_back.setClickedListener(component -> onBackPressed());
        // 修改信息按钮
        Image edit = (Image) findComponentById(ResourceTable.Id_tb_page_title_edit);
        edit.setClickedListener(component -> {
            // 修改看板信息
            CommonDialog dialog = new CommonDialog(getContext());
            Component content_container = LayoutScatter.getInstance(getContext())
                    .parse(ResourceTable.Layout_edit_layout_custom_dialog_content,
                            null, false);
            dialog.setContentCustomComponent(content_container);
            dialog.setSize(MATCH_CONTENT, MATCH_CONTENT);
            // 设置色卡状态
            RadioContainer radioContainer = (RadioContainer)
                    content_container.findComponentById(ResourceTable.Id_edit_input_color_select);
            int count = radioContainer.getChildCount();
            for (int i = 0; i < count; i++) {
                ((RadioButton) radioContainer.getComponentAt(i)).setButtonElement(createStateElement(i));
            }
            // 设置默认内容，默认为看板的信息
            TextField textField_title = content_container.findComponentById(ResourceTable.Id_add_page_input_title);
            textField_title.setText(tBoard.getTitle());
            TextField textField_content = content_container.findComponentById(ResourceTable.Id_edit_input_content);
            textField_content.setText(tBoard.getDescribe());
            radioContainer.setMarkChangedListener((radioContainer1, index) -> {
                change_color = index;
            });
            // 写入数据库
            Button button = (Button) content_container.findComponentById(ResourceTable.Id_add_page_button);
            button.setClickedListener(component1 -> {
                tBoard.setTitle(textField_title.getText());
                tBoard.setDescribe(textField_content.getText());
                tBoard.setColor(change_color);
                changeTBoardInfo();
                String tBTitle2 = tBoard.getTitle();
                if (tBTitle2.equals("")) {
                    bg_title.setText("（无标题）");
                    bg_title.setTextColor(new Color(0xFF8C8888));
                } else {
                    bg_title.setText(tBTitle2);
                }
                dialog.destroy();
            });
            dialog.setButton(IDialog.BUTTON1, "删除看板", (iDialog, i) -> {
                delTaskBoard();
                onBackPressed();
                iDialog.destroy();
            });
            dialog.setButton(IDialog.BUTTON2, "取消", (iDialog, i) -> iDialog.destroy());
            dialog.show();
        });
        // 设置 页面 信息
        pageSlider = (PageSlider) findComponentById(ResourceTable.Id_tb_page_slider);
        // 初始化 内容信息
        initPage();
        // 清理提示语。
        cleanText();
        // 将初始化完毕的 taskBoardPageProvider 和 PageSlider 绑定。
        pageSlider.setProvider(taskBoardPageProvider);
        // 启用回弹效果
        pageSlider.setReboundEffect(true);
        // 底部滑块。
        initBottom();
        // 添加页面
        Image addPage = (Image) findComponentById(ResourceTable.Id_tb_page_title_add);
        addPage.setClickedListener(component -> {
            CommonDialog dialog = new CommonDialog(getContext());
            Component content_container = LayoutScatter.getInstance(getContext())
                    .parse(ResourceTable.Layout_add_page_dialog,
                            null, false);
            // 获取页面标题
            TextField textField_input_title = (TextField) content_container.findComponentById(ResourceTable.Id_add_page_input);
            Button addPageButton = (Button) content_container.findComponentById(ResourceTable.Id_add_page_button);
            addPageButton.setClickedListener(component1 -> {
                if (textField_input_title.getText().equals("")) {
                    dialog.destroy();
                } else {
                    add_page(textField_input_title.getText());
                }
                // 清理提示语
                cleanText();
                // 初始化页面按钮
                initButton();
                dialog.destroy();
            });
            dialog.setContentCustomComponent(content_container);
            dialog.setSize(MATCH_CONTENT, MATCH_CONTENT);
            dialog.setButton(IDialog.BUTTON1, "取消", (iDialog, i) -> iDialog.destroy());
            dialog.show();
        });
    }

    private void delTaskBoard() {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext delAllOrmContext = helper.getOrmContext("Htk");
        OrmPredicates queryCard = delAllOrmContext.where(Card.class).equalTo("tBoardId", tBoardId);
        List<Card> cardList = delAllOrmContext.query(queryCard);
        OrmPredicates queryPage = delAllOrmContext.where(Pages.class).equalTo("tBoardId", tBoardId);
        List<Pages> pageList = delAllOrmContext.query(queryPage);
        OrmPredicates queryTaskBoard = delAllOrmContext.where(TBoard.class).equalTo("id", tBoardId);
        List<TBoard> pageTaskBoard = delAllOrmContext.query(queryTaskBoard);
        for (Card card : cardList) {
            delAllOrmContext.delete(card);
            delAllOrmContext.flush();
        }
        for (Pages pages : pageList) {
            delAllOrmContext.delete(pages);
            delAllOrmContext.flush();
        }
        for (TBoard tBoard : pageTaskBoard) {
            delAllOrmContext.delete(tBoard);
            delAllOrmContext.flush();
        }
    }

    // 底部滑块
    private void initBottom() {
        // 底部页面导航栏
        PageSliderIndicator indicator = (PageSliderIndicator) findComponentById(ResourceTable.Id_indicator);
        ShapeElement normalElement = new ShapeElement();
        normalElement.setRgbColor(RgbPalette.BLACK);
        normalElement.setAlpha(168);
        normalElement.setShape(ShapeElement.OVAL);
        normalElement.setBounds(0, 0, 32, 32);
        ShapeElement selectedElement = new ShapeElement();
        selectedElement.setRgbColor(RgbPalette.WHITE);
        selectedElement.setAlpha(168);
        selectedElement.setShape(ShapeElement.OVAL);
        selectedElement.setBounds(0, 0, 48, 48);
        indicator.setItemElement(normalElement, selectedElement);
        indicator.setItemOffset(60);
        indicator.setPageSlider(pageSlider);
    }

    private void initPage() {
        // 初始化所需的变量
        pages = new ArrayList<Pages>();
        listCards = new ArrayList<List<Card>>();
        page_list = new ArrayList<TaskBoardPageProvider.TbPage>();
        // 获取变量的值。，保证值 对象的唯一性。
        copyPagesFromDB(getPagesFromDB());
        for (Pages page : pages) {
            copyCardFromDB(getPageCardFromDB(page.getId()));
        }
        // 将两个值进行包装入 page_list，需要 pages、listContainer（card），两样。包装好 page_list。
        bindPageCard();
        // 数据和 PageProvider 绑定。
        taskBoardPageProvider = new TaskBoardPageProvider(page_list, this, tBoardId);
    }

    // 绑定页面和卡片，逐个获取卡片列表，和 cardProvider 绑定, cardProvider 和 card_listContainer 绑定
    private void bindPageCard() {
        for (int i = 0; i < pages.size(); i++) {
            // 卡片数据源
            CardProvider cardProvider = new CardProvider(listCards.get(i), this);
            // 列表视图
            ListContainer card_listContainer = new ListContainer(null);
            // 绑定数据源
            card_listContainer.setItemProvider(cardProvider);
            // 列表视图添加监听
            int pageIndex = i;
            card_listContainer.setItemClickedListener((container, component, position, id) -> {
                Card card = (Card) card_listContainer.getItemProvider().getItem(position);
                Intent intent = new Intent();
                intent.setParam("pageId", pages.get(pageIndex).getId());
                intent.setParam("tBoardId", tBoardId);
                intent.setParam("cardId", card.getId());
                Operation operation = new Intent.OperationBuilder().withDeviceId("")
                        .withBundleName(getBundleName())
                        .withAbilityName(CardDetailAbility.class.getName())
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
            });
            // 关闭长按
            card_listContainer.setLongClickable(false);
            // 将 卡片 装进 listContainer 后，装进 TbPage, 再装进
            page_list.add(new TaskBoardPageProvider.TbPage(pages.get(i), card_listContainer));
        }
    }

    private void add_page(String pageName) {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext add_ormContext = helper.getOrmContext("Htk");
        // 首先，将新增的页面加入数据库，先初始化对象
        Pages add_page = new Pages(pageName, Integer.parseInt(tBoardId));
        add_ormContext.insert(add_page);
        add_ormContext.flush();
        OrmPredicates query = helper.getOrmContext("Htk").where(Pages.class).equalTo("tBoardId", tBoardId)
                .and().equalTo("title", pageName);
        add_page = (Pages) helper.getOrmContext("Htk").query(query).get(0);
        // 再加入变量中，首先加入 Pages
        pages.add(add_page);
        // 再构造 listCard，并加入到 listCards 中。
        List<Card> addListCard = new ArrayList<Card>();
        listCards.add(addListCard);
        // 再构造 listContainer，用于装卡片
        CardProvider addCardProvider = new CardProvider(addListCard, TaskBoardAbilitySlice.this);
        ListContainer new_add_listContainer = new ListContainer(null);
        // 不添加卡片的点击事件监听，后续会覆盖。
        new_add_listContainer.setItemProvider(addCardProvider);
        // 构造 TbPage 装入 page_list
        page_list.add(new TaskBoardPageProvider.TbPage(add_page, new_add_listContainer));
        // 唤起 taskBoardPageProvider，改变数据。
        taskBoardPageProvider.listButton.clear();
        taskBoardPageProvider.notifyDataChanged();
    }

    // 保存卡片信息。
    private void copyCardFromDB(List<Card> list_card) {
        listCards.add(list_card);
    }

    /**
     * 从数据库赋值页面。
     *
     * @param temp_pagesFromDB 数据库查询到 Pages 页面集合。
     */
    private void copyPagesFromDB(List<Pages> temp_pagesFromDB) {
        pages.clear();
        pages.addAll(temp_pagesFromDB);
    }

    /**
     * @return 返回 属于此看板的 页面 的 卡片。
     */
    private List<Card> getPageCardFromDB(Integer pageId) {
        // 数据库操作
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext getPageCardFromDB_htk = helper.getOrmContext("Htk");
        OrmPredicates query = getPageCardFromDB_htk.where(Card.class).equalTo("tBoardId", tBoardId)
                .and().equalTo("pageId", pageId);
        List<Card> res = getPageCardFromDB_htk.query(query);
        return res;
    }

    /**
     * 清理警告文字
     */
    private void cleanText() {
        if (page_list.size() != 0) {
            Text tBNullDataText = (Text) findComponentById(ResourceTable.Id_tb_null_data_text);
            tBNullDataText.setText("");
        }
    }

    /**
     * @return 返回 属于此看板的 页面
     */
    private List<Pages> getPagesFromDB() {
        // 数据库操作
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext getPagesFromDB_htk = helper.getOrmContext("Htk");
        OrmPredicates query = getPagesFromDB_htk.where(Pages.class).equalTo("tBoardId", Integer.parseInt(tBoardId));
        return getPagesFromDB_htk.query(query);
    }

    /**
     * 修改 看板 信息
     */
    private void changeTBoardInfo() {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext changeTBoardInfo_htk = helper.getOrmContext("Htk");
        changeTBoardInfo_htk.update(tBoard);
        changeTBoardInfo_htk.flush();
    }

    /**
     * @return 返回传过来的 所属面板
     */
    private TBoard getTBoard() {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext getTBoard_htk = helper.getOrmContext("Htk");
        OrmPredicates query = getTBoard_htk.where(TBoard.class).equalTo("id", tBoardId);
        List<TBoard> list = getTBoard_htk.query(query);
        return list.get(0);
    }

    // 设置 色卡的状态
    private StateElement createStateElement(int i) {
        RgbColor rgbColor = null;
        if (i == 0) {
            rgbColor = new RgbColor(207, 197, 226);
        } else if (i == 1) {
            rgbColor = new RgbColor(255, 184, 179);
        } else if (i == 2) {
            rgbColor = new RgbColor(181, 204, 141);
        } else if (i == 3) {
            rgbColor = new RgbColor(166, 186, 221);
        } else {
            rgbColor = new RgbColor(241, 215, 159);
        }

        // 按下的状态
        ShapeElement elementButtonOn = new ShapeElement();
        elementButtonOn.setRgbColor(rgbColor);
        elementButtonOn.setStroke(8, RgbPalette.WHITE);
        elementButtonOn.setShape(ShapeElement.RECTANGLE);

        // 正常状态
        ShapeElement elementButtonOff = new ShapeElement();
        elementButtonOff.setRgbColor(rgbColor);
        elementButtonOff.setShape(ShapeElement.RECTANGLE);
        elementButtonOff.setCornerRadius(8.0f);

        // 状态绑定
        StateElement checkElement = new StateElement();
        checkElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, elementButtonOn);
        checkElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, elementButtonOff);
        return checkElement;
    }

    private void createDialog(String content) {
        CommonDialog dialog = new CommonDialog(getContext());
        Component container = LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_fail_warning, null, false);
        Text text = container.findComponentById(ResourceTable.Id_fail_content);
        text.setText(content);
        dialog.setContentCustomComponent(container);
        dialog.setSize(MATCH_CONTENT, MATCH_CONTENT);
        dialog.setButton(IDialog.BUTTON3, "已知晓", (iDialog, i) -> iDialog.destroy());
        dialog.show();
    }

    @Override
    public void onActive() {
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.WHITE.getValue()); // 设置状态栏颜色
        super.onActive();
        // 更新所有信息
        upDataAll();
    }

    // 更新所有信息
    private void upDataAll() {
        // 更新页面信息
        copyPagesFromDB(getPagesFromDB());
        // 更新卡片信息
        listCards.clear();
        for (Pages page : pages) {
            copyCardFromDB(getPageCardFromDB(page.getId()));
        }
        // 清空 pageList，pageList 对象也随之改变。
        page_list.clear();
        // 绑定信息
        bindPageCard();
        // 全部更新
        taskBoardPageProvider.listButton.clear();
        // 重新绑定 pageList 和 taskBoardPageProvider
        taskBoardPageProvider = new TaskBoardPageProvider(page_list, this, tBoardId);
        // 重新绑定 taskBoardPageProvider 和 pageSlider
        pageSlider.setProvider(taskBoardPageProvider);
        taskBoardPageProvider.notifyDataChanged();
        // 初始化页面按钮
        initButton();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}