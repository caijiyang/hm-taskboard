package com.felixcjy.hmtaskboard.slice;

import com.felixcjy.hmtaskboard.ResourceTable;
import com.felixcjy.hmtaskboard.pojo.TUser;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.WindowManager;
import ohos.data.DatabaseHelper;
import ohos.data.orm.OrmContext;
import ohos.data.orm.OrmPredicates;

import java.util.List;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_CONTENT;

/**
 * @author 蔡济阳（FelixCJY / FelixCai）
 * @date 2022/05/01 19:35
 **/

public class ForgetPasswordAbilitySlice extends AbilitySlice {

    TextField user_id;
    TextField user_phone;
    TextField new_password;
    TextField new_password_again;
    Text show_password;
    Button confirm_btn;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_forget_password);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(0xFFE4EFFF); // 设置状态栏颜色
        user_id = findComponentById(ResourceTable.Id_forget_user_id);
        user_phone = findComponentById(ResourceTable.Id_forget_user_phone);
        new_password = findComponentById(ResourceTable.Id_forget_new_password);
        new_password_again = findComponentById(ResourceTable.Id_forget_new_password_again);
        show_password = findComponentById(ResourceTable.Id_forget_show_password);
        confirm_btn = findComponentById(ResourceTable.Id_forget_confirm_btn);
        initForgetPasswordAbilitySlice();
    }

    private void initForgetPasswordAbilitySlice() {
        // 显示密码
        show_password.setClickedListener(component -> {
            if (show_password.getText().equals("显示密码")) {
                new_password.setTextInputType(InputAttribute.PATTERN_TEXT);
                new_password_again.setTextInputType(InputAttribute.PATTERN_TEXT);
                show_password.setText("隐藏密码");
            } else {
                new_password.setTextInputType(InputAttribute.PATTERN_PASSWORD);
                new_password_again.setTextInputType(InputAttribute.PATTERN_PASSWORD);
                show_password.setText("显示密码");
            }
        });
        // 确认按钮
        confirm_btn.setClickedListener(component -> {
            if (new_password.getText().equals("")) {
                createDialog("请输入密码");
            }
            if (new_password_again.getText().equals("")) {
                createDialog("请再次输入密码");
            }
            if (new_password.getText().equals(new_password_again.getText())) {
                if (operationUserInfoFromDB("read", user_id.getText(), user_phone.getText(), "")) {
                    operationUserInfoFromDB("change", user_id.getText(), user_phone.getText(), new_password.getText());
                    DirectionalLayout toastDialog_layout = (DirectionalLayout) LayoutScatter.getInstance(this)
                            .parse(ResourceTable.Layout_toast_dialog_grayback, null, false);
                    Text text_show = toastDialog_layout.findComponentById(ResourceTable.Id_msg_toast);
                    text_show.setText("密码修改成功");
                    new ToastDialog(getContext())
                            .setContentCustomComponent(toastDialog_layout)
                            .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                            .setAlignment(LayoutAlignment.CENTER)
                            .show();
                    Intent intent1 = new Intent();
                    intent1.setParam("phone", user_phone.getText());
                    intent1.setParam("password", new_password.getText());
                    present(new LoginAbilitySlice(), intent1);
                    this.terminate();
                } else {
                    createDialog("未找到此人，请检查【手机号】或者【ID】是否有误。");
                }
            } else {
                createDialog("两次密码输入不一致");
            }
        });
    }

    private boolean operationUserInfoFromDB(String operation, String id, String phone,String password) {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext getCardOrmContext = helper.getOrmContext("Htk");
        OrmPredicates query = getCardOrmContext.where(TUser.class)
                .equalTo("id", id).equalTo("phone", phone);
        List<TUser> tUsers = getCardOrmContext.query(query);
        if (operation.equals("read")) {
            return tUsers.size() != 0;
        } else if (operation.equals("change")) {
            TUser tUser = tUsers.get(0);
            tUser.setPassword(password);
            getCardOrmContext.flush();
            return true;
        }
        return false;
    }

    private void createDialog(String content) {
        CommonDialog dialog = new CommonDialog(getContext());
        Component container = LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_fail_warning, null, false);
        Text text = container.findComponentById(ResourceTable.Id_fail_content);
        text.setText(content);
        dialog.setContentCustomComponent(container);
        dialog.setSize(MATCH_CONTENT, MATCH_CONTENT);
        dialog.setButton(IDialog.BUTTON3, "已知晓", (iDialog, i) -> iDialog.destroy());
        dialog.show();
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onBackground() {
        super.onBackground();
    }
}
