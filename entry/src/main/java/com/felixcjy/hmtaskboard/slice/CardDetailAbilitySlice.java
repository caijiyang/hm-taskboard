package com.felixcjy.hmtaskboard.slice;

import com.felixcjy.hmtaskboard.IShareIdlInterface;
import com.felixcjy.hmtaskboard.ResourceTable;
import com.felixcjy.hmtaskboard.ShareIdlInterfaceStub;
import com.felixcjy.hmtaskboard.ShareServiceAbility;
import com.felixcjy.hmtaskboard.pojo.Card;
import com.felixcjy.hmtaskboard.provider.DeviceListProvider;
import com.felixcjy.hmtaskboard.utils.NotificationUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.IAbilityConnection;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.service.WindowManager;
import ohos.bundle.ElementName;
import ohos.data.DatabaseHelper;
import ohos.data.orm.OrmContext;
import ohos.data.orm.OrmPredicates;
import ohos.distributedschedule.interwork.DeviceInfo;
import ohos.distributedschedule.interwork.DeviceManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.photokit.metadata.AVStorage;
import ohos.rpc.IRemoteObject;
import ohos.rpc.RemoteException;
import ohos.utils.IntentConstants;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_CONTENT;

/**
 * @author 蔡济阳（FelixCJY / FelixCai）
 * @date 2022/04/17 20:10
 **/

public class CardDetailAbilitySlice extends AbilitySlice {

    private static final HiLogLabel HiLog_Label = new HiLogLabel(HiLog.LOG_APP, 0, "hmtb_CardDetailAbilitySlice");
    private static final String HiLog_Format = "%{public}s";

    Integer pageId;
    Integer tBoardId;
    Integer cardId;

    Text card_title; // 卡片名称
    Text card_add_common; // 添加卡片描述
    Text card_due_date; // 添加卡片到期日
    Switch expiration_reminder_switch; // 开启签到提醒
    Text card_cover_setImg; // 设置封面图字样
    Image card_back_btn_img; // 返回按钮

    String card_date = "";
    String card_time = "";
    int reminderId = -1;
    int reminderKey = -1; // 关闭 switch 是否需要删除通知，0 不需要， 1 需要，-1 初始值，不需要。

    DirectionalLayout directionalLayout;
    int imgRequestCode = 1234;
    String chooseImgId;
    String cover_src = "";

    Button delete_card;

    Image share_card;

    @Override
    protected void onStart(Intent intent) {
        reminderKey = -1;
        HiLog.info(HiLog_Label, HiLog_Format, "----------------------");
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_card_detail);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(0xFFE4EFFF); // 设置状态栏颜色
        pageId = intent.getIntParam("pageId", 1);
        tBoardId = intent.getIntParam("tBoardId", 1);
        cardId = intent.getIntParam("cardId", 0);
        card_title = findComponentById(ResourceTable.Id_card_detail_title);
        card_add_common = findComponentById(ResourceTable.Id_card_detail_add_common);
        card_due_date = findComponentById(ResourceTable.Id_card_detail_due_date);
        expiration_reminder_switch = findComponentById(ResourceTable.Id_expiration_reminder);
        card_cover_setImg = findComponentById(ResourceTable.Id_card_detail_cover);
        card_back_btn_img = findComponentById(ResourceTable.Id_card_back);
        directionalLayout = findComponentById(ResourceTable.Id_card_img_layout);
        delete_card = findComponentById(ResourceTable.Id_cared_detail_delete_card_btn);
        share_card = findComponentById(ResourceTable.Id_share_btn);
        initCardDetailAbilitySlice();
        addComponentListener();
    }

    private void initCardDetailAbilitySlice() {
//        HiLog.info(HiLog_Label, HiLog_Format, "card 的ID是：" + cardId);
//        HiLog.info(HiLog_Label, HiLog_Format, "测试title：" + getCardInfo("title"));
//        HiLog.info(HiLog_Label, HiLog_Format, "测试describe：" + getCardInfo("describe"));
//        HiLog.info(HiLog_Label, HiLog_Format, "测试时间：" + getCardInfo("dueTime"));
//        HiLog.info(HiLog_Label, HiLog_Format, "测试封面图：" + getCardInfo("imgSrc"));
//        HiLog.info(HiLog_Label, HiLog_Format, "测试开关：" + getCardInfo("switch"));

        String imgSrc_1 = getCardInfo("imgSrc");
        String title_1 = getCardInfo("title");
        String describe_1 = getCardInfo("describe");
        String dueTime_1 = getCardInfo("dueTime");
        String switch_1 = getCardInfo("switch");

        // 初始化卡片名称
        if (!(title_1 == null)) {
            card_title.setText(title_1);
        }
        // 初始化卡片描述
        if (!(describe_1 == null) && !describe_1.equals("")) {
            card_add_common.setText(describe_1);
        } else {
            card_add_common.setText("添加卡片描述");
        }
        // 初始化提醒时间 和 切换按钮
        expiration_reminder_switch.setCheckedStateChangedListener((absButton, isChecked) -> {});
        // 初始化时间
        if (!(dueTime_1 == null)) {
            card_due_date.setText(dueTime_1);
            // 初始化提醒时间
            String[] timeAndDay = dueTime_1.split("-");
            timeAndDay[0] = reFormTime(timeAndDay[0],"date");
            card_date = timeAndDay[0];
            timeAndDay[1] = reFormTime(timeAndDay[1],"time");
            card_time = timeAndDay[1];
            // 检查是否过期了，同时保证通知是有效的。
            if (isRightTime(timeAndDay[0].replace('.', '-') + " " + timeAndDay[1] + ":00")) {
                if (!(switch_1 == null)) {
                    if (switch_1.equals("1")) {
                        HiLog.info(HiLog_Label, HiLog_Format, "没过期，有通知");
                        reminderKey = 1;
                        expiration_reminder_switch.setChecked(true);
                    }
                } else {
                    HiLog.info(HiLog_Label,HiLog_Format,"没过期，无通知");
                    reminderKey = 0;
                    expiration_reminder_switch.setChecked(false);
                }
            } else {
                HiLog.info(HiLog_Label, HiLog_Format, "过期了");
                // 过期了。删除过期通知。
                reminderKey = 0;
                expiration_reminder_switch.setChecked(false);
                expiration_reminder_switch.setChecked(false);
                delDelayDayTime();
            }
        } else {
            HiLog.info(HiLog_Label, HiLog_Format, "什么都没有");
            card_due_date.setText("添加到期日");
        }
        // 初始化提醒开关按钮，先清除原本监听内容，再设置，下一步再打开监听。
        expiration_reminder_switch.setChecked(!(switch_1 == null) && switch_1.equals("1"));
        // 初始化封面图
        if (!(imgSrc_1 == null) && !(imgSrc_1.equals(""))) {
            String[] img1 = getCardInfo("imgSrc").split("-");
            setCardImg(Uri.parse(img1[1]), "read");
        } else {
            directionalLayout.removeAllComponents();
        }
    }

    private String reFormTime(String time, String type) {
        if (type.equals("date")) {
            String[] times = time.split("\\.");
            String mon = times[1];
            String day = times[2];
            if (Integer.parseInt(times[1]) < 10) {
                mon = "0" + Integer.parseInt(times[1]);
            }
            if (Integer.parseInt(times[2]) < 10) {
                day = "0" + Integer.parseInt(times[2]);
            }
            time = times[0] + "." + mon + "." + day;
        } else if (type.equals("time")) {
            String[] times = time.split(":");
            String hour = times[0];
            String min = times[1];
            if (Integer.parseInt(times[0]) < 10) {
                hour = "0" + Integer.parseInt(times[0]);
            }
            if (Integer.parseInt(times[1]) < 10) {
                min = "0" + Integer.parseInt(times[1]);
            }
            time = hour + ":" + min;
        }
        return time;
    }

    private void addComponentListener() {
        // 返回按钮
        card_back_btn_img.setClickedListener(component -> onBackPressed());
        // 标题
        card_title.setClickedListener(component -> {
            CommonDialog dialog = new CommonDialog(this);
            Component content_container = LayoutScatter.getInstance(this)
                    .parse(ResourceTable.Layout_card_set_title, null, false);
            TextField card_set_title = content_container.findComponentById(ResourceTable.Id_card_set_title_cancel_card_title);
            card_set_title.setText(getCardInfo("title"));
            Button cancel = content_container.findComponentById(ResourceTable.Id_card_set_title_cancel);
            cancel.setClickedListener(component1 -> dialog.destroy());
            Button confirm = content_container.findComponentById(ResourceTable.Id_card_set_title_confirm);
            confirm.setClickedListener(component1 -> {
                if (card_set_title.getText().equals("")) {
                    createDialog("卡片名称不能为空");
                } else {
                    card_title.setText(card_set_title.getText());
                    setTitleFromDB(card_set_title.getText());
                    dialog.destroy();
                }
            });
            dialog.setContentCustomComponent(content_container);
            dialog.setSize(MATCH_CONTENT, MATCH_CONTENT);
            dialog.show();
        });
        // 描述
        card_add_common.setClickedListener(component -> {
            CommonDialog dialog = new CommonDialog(this);
            Component content_container = LayoutScatter.getInstance(this)
                    .parse(ResourceTable.Layout_card_set_title, null, false);
            TextField card_set_title = content_container.findComponentById(ResourceTable.Id_card_set_title_cancel_card_title);
            Text title = content_container.findComponentById(ResourceTable.Id_card_set_title_title);
            title.setText("设置卡片描述");
            card_set_title.setHint("卡片描述");
            card_set_title.setText(getCardInfo("describe"));
            Button cancel = content_container.findComponentById(ResourceTable.Id_card_set_title_cancel);
            cancel.setClickedListener(component1 -> dialog.destroy());
            Button confirm = content_container.findComponentById(ResourceTable.Id_card_set_title_confirm);
            confirm.setClickedListener(component1 -> {
                if (card_set_title.getText().equals("")) {
                    card_add_common.setText("添加卡片描述");
                    setCommonFromDB("");
                } else {
                    card_add_common.setText(card_set_title.getText());
                    setCommonFromDB(card_set_title.getText());
                }
                dialog.destroy();
            });
            dialog.setContentCustomComponent(content_container);
            dialog.setSize(MATCH_CONTENT, MATCH_CONTENT);
            dialog.show();
        });
        // 设置卡片延期时间。
        card_due_date.setClickedListener(component -> {
            CommonDialog dialog = new CommonDialog(this);
            Component content_container = LayoutScatter.getInstance(this)
                    .parse(ResourceTable.Layout_card_set_due_date, null, false);
            Calendar cal = Calendar.getInstance();
            int y = cal.get(Calendar.YEAR);
            int m = cal.get(Calendar.MONTH) + 1;
            String m1;
            if (m < 10) {
                m1 = "0" + m;
            } else {
                m1 = m + "";
            }
            int d = cal.get(Calendar.DATE);
            int h = cal.get(Calendar.HOUR_OF_DAY);
            int mi = cal.get(Calendar.MINUTE);
            String mi1;
            if (mi < 10) {
                mi1 = "0" + mi;
            } else {
                mi1 = mi + "";
            }
            // 日期
            DatePicker datePicker = content_container.findComponentById(ResourceTable.Id_card_set_due_date_date_pick);
            Text dateFromPicker = content_container.findComponentById(ResourceTable.Id_card_set_due_date_date);
            dateFromPicker.setText(y + "." + m1 + "." + d);
            card_date = dateFromPicker.getText();
            Date date = new Date();
            long unixTime = date.getTime() / 1000L;
            datePicker.setMinDate(unixTime);
            datePicker.setValueChangedListener((datePicker1, year, monthOfYear, dayOfMonth) -> {
                dateFromPicker.setText(String.format("%4d.%02d.%02d", year, monthOfYear, dayOfMonth));
                card_date = dateFromPicker.getText();
            });
            // 时间
            TimePicker timePicker = content_container.findComponentById(ResourceTable.Id_card_set_due_date_time_picker);
            timePicker.showSecond(false);
            Text timeFromPicker = content_container.findComponentById(ResourceTable.Id_card_set_due_date_time);
            timeFromPicker.setText(h + ":" + mi1);
            card_time = timeFromPicker.getText();
            timePicker.setTimeChangedListener((timePicker1, hour, minute, second) -> {
                timeFromPicker.setText(String.format("%02d:%02d", hour, minute));
                card_time = timeFromPicker.getText();
            });
            // 确认和取消按钮
            Button confirm_btn = content_container.findComponentById(ResourceTable.Id_card_set_due_date_confirm);
            confirm_btn.setClickedListener(component1 -> {
                card_due_date.setText(card_date + "-" + card_time);
                setDueTimeToDB(card_due_date.getText());
                dialog.destroy();
            });
            Button cancel_btn = content_container.findComponentById(ResourceTable.Id_card_set_due_date_cancel);
            cancel_btn.setClickedListener(component1 -> dialog.destroy());
            dialog.setContentCustomComponent(content_container);
            dialog.setSize(MATCH_CONTENT, MATCH_CONTENT);
            dialog.show();
        });
        // 开启提醒按钮
        expiration_reminder_switch.setCheckedStateChangedListener((absButton, isChecked) -> {
            if (isChecked) {
                // 由关到开，创建通知，检查时间是否符合条件
                if (!card_date.equals("") && !(card_time.equals("")) && !card_due_date.getText().equals("添加到期日")) {
                    card_date = reFormTime(card_date,"date");
                    card_time = reFormTime(card_time,"time");
                    if (isRightTime(card_date.replace('.', '-') + " " + card_time + ":00")) {
//                        HiLog.info(HiLog_Label,HiLog_Format,"时间符合条件");
                        String[] card_date_split = card_date.split("\\.");
                        String[] card_time_split = card_time.split(":");
                        String content;
                        if (getCardInfo("describe") == null) {
                            content = "（该卡片无描述）";
                        } else {
                            content = getCardInfo("describe");
                        }
//                        HiLog.info(HiLog_Label,HiLog_Format,"年："+Integer.valueOf(card_date_split[0]));
//                        HiLog.info(HiLog_Label,HiLog_Format,"月："+Integer.valueOf(card_date_split[1]));
//                        HiLog.info(HiLog_Label,HiLog_Format,"日："+Integer.valueOf(card_date_split[2]));
//                        HiLog.info(HiLog_Label,HiLog_Format,"时："+Integer.valueOf(card_time_split[0]));
//                        HiLog.info(HiLog_Label,HiLog_Format,"分："+Integer.valueOf(card_time_split[1]));
//                        HiLog.info(HiLog_Label,HiLog_Format,"标题："+"来自卡片：" + getCardInfo("title"));
//                        HiLog.info(HiLog_Label,HiLog_Format,"内容："+content);
                        reminderId = NotificationUtils.createNotification(Integer.valueOf(card_date_split[0]), Integer.valueOf(card_date_split[1]),
                                Integer.valueOf(card_date_split[2]), Integer.valueOf(card_time_split[0]),
                                Integer.valueOf(card_time_split[1])
                                , "来自卡片：" + getCardInfo("title"), content);
                        reminderKey = 1;
                        addNotificationToDB(card_date + "-" + card_time, reminderId);
                    } else {
                        createDialog("日期不符合条件：大于当前的时间点。请重新选择日期。");
                        reminderKey = 0;
                        expiration_reminder_switch.setChecked(false);
                    }
                } else {
                    createDialog("请先设置【到期日】。");
                    reminderKey = 0;
                    expiration_reminder_switch.setChecked(false);
                }
            } else {
                //由开到关，删除通知，首先检查是否存在通知
                if (getCardIsSwitchFromDB() && reminderKey == 1) {
                    // 有通知，关闭通知
                    reminderKey = 0;
                    delDelayDayTime();
                    NotificationUtils.closeNotification(Integer.parseInt(getCardInfo("reminderId")));
                }
            }
        });

        // 设置封面图
        card_cover_setImg.setClickedListener(component -> {
            card_cover_setImg.setText("更改图片");
            Intent intent = new Intent();
            Operation opt = new Intent.OperationBuilder().withAction(IntentConstants.ACTION_CHOOSE).build();
            intent.setOperation(opt);
            intent.addFlags(Intent.FLAG_NOT_OHOS_COMPONENT);
            intent.setType("image/*");
            startAbilityForResult(intent, imgRequestCode);
        });
        // 退出按钮
        delete_card.setClickedListener(component -> {
            delCardFromDB();
            onBackPressed();
        });
        // 分享
        share_card.setClickedListener(component -> {
            CommonDialog commonDialog = new CommonDialog(this);
            // 参数表示不同状态的设备
            List<DeviceInfo> deviceInfoList = DeviceManager.getDeviceList(DeviceInfo.FLAG_GET_ONLINE_DEVICE);
            DeviceListProvider deviceListProvider = new DeviceListProvider(deviceInfoList,this);
            ListContainer listContainer = new ListContainer(this);
            listContainer.setItemProvider(deviceListProvider);
            listContainer.setItemClickedListener((listContainer1, component1, position, id) -> {
                // 获取设备
                DeviceInfo deviceInfo = deviceInfoList.get(position);
                String deviceId = deviceInfo.getDeviceId();
                // 远程连接
                Intent connectIntent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName(getBundleName())
                        .withAbilityName(ShareServiceAbility.class.getName())
                        .withDeviceId(deviceId)
                        .withFlags(Intent.FLAG_ABILITYSLICE_MULTI_DEVICE)
                        .build();
                commonDialog.destroy();
                connectIntent.setOperation(operation);
                connectAbility(connectIntent, new IAbilityConnection() {
                    @Override
                    public void onAbilityConnectDone(ElementName elementName, IRemoteObject iRemoteObject, int i) {
                        HiLog.info(HiLog_Label,HiLog_Format,"连接成功");
                        // 连接成功，获取远程接口
                        IShareIdlInterface myBinder = ShareIdlInterfaceStub.asInterface(iRemoteObject);
                        try {
                            myBinder.trans(pageId,tBoardId,cardId);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onAbilityDisconnectDone(ElementName elementName, int i) {
                        // 连接失败
                        HiLog.info(HiLog_Label,HiLog_Format,"连接失败");
                    }
                });
            });

            commonDialog.setSize(1000, 800);
            Component container = LayoutScatter.getInstance(getContext())
                    .parse(ResourceTable.Layout_share_device_dialog, null, false);
            if (!(deviceInfoList.size() == 0)) {
                DirectionalLayout directionalLayout_text = container.findComponentById(ResourceTable.Id_show_device_list_text);
                directionalLayout_text.removeAllComponents();
            }
            DirectionalLayout directionalLayout = container.findComponentById(ResourceTable.Id_show_dialog_bg);
            directionalLayout.addComponent(listContainer);
            Text close = container.findComponentById(ResourceTable.Id_show_dialog_close);
            close.setClickedListener(component1 -> commonDialog.destroy());
            commonDialog.setContentCustomComponent(container);
            commonDialog.setButton(IDialog.BUTTON3, "取消", (iDialog, i) -> iDialog.destroy());
            commonDialog.show();
        });
    }


    // 删除过期通知
    private void delDelayDayTime() {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext setCardOrmContext = helper.getOrmContext("Htk");
        Card delCardDelayDayTime = getCard();
        delCardDelayDayTime.setIsSwitch("0");
        setCardOrmContext.update(delCardDelayDayTime);
        setCardOrmContext.flush();
    }

    // 删除卡片
    private void delCardFromDB() {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext setCardOrmContext = helper.getOrmContext("Htk");
        Card delCard = getCard();
        setCardOrmContext.delete(delCard);
        setCardOrmContext.flush();
    }

    // 通知添加进入数据库
    private void addNotificationToDB(String dueTime, int reminderId) {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext setCardOrmContext = helper.getOrmContext("Htk");
        Card updateCardTime = getCard();
        updateCardTime.setDueTime(dueTime);
        updateCardTime.setReminderId(reminderId);
        updateCardTime.setIsSwitch("1");
        setCardOrmContext.update(updateCardTime);
        setCardOrmContext.flush();
    }

    // 检查时间是否符合条件，比当前时间大。
    private boolean isRightTime(String time) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date t = dateFormat.parse(time);
//            HiLog.info(HiLog_Label,HiLog_Format,"所获时间是："+time);
            //获取当前时间
            Calendar date = Calendar.getInstance();
            date.setTime(new Date());
            Calendar end = Calendar.getInstance();
            end.setTime(t);
            //String转Date
            if (date.before(end)) {
//                HiLog.info(HiLog_Label, HiLog_Format, "当前时间 在 所选时间之前");
                return true;
            }
        } catch (Exception e) {
//            HiLog.info(HiLog_Label,HiLog_Format,"出现异常：");
        }
        return false;
    }

    // 查看是否存在通知
    private boolean getCardIsSwitchFromDB() {
        if (getCardInfo("switch") == null) {
            return false;
        } else {
            return getCardInfo("switch").equals("1");
        }
    }

    // 设置提醒时间到数据库
    private void setDueTimeToDB(String timeToDB) {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext setCardOrmContext = helper.getOrmContext("Htk");
        Card updateCardTime = getCard();
        updateCardTime.setDueTime(timeToDB);
        setCardOrmContext.update(updateCardTime);
        setCardOrmContext.flush();
    }

    // 设置卡片描述到数据库
    private void setCommonFromDB(String common) {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext setCardOrmContext = helper.getOrmContext("Htk");
        Card updateCardDescribe = getCard();
        updateCardDescribe.setDescribe(common);
        setCardOrmContext.update(updateCardDescribe);
        setCardOrmContext.flush();
    }

    // 设置标题到数据库
    private void setTitleFromDB(String title) {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext setCardOrmContext = helper.getOrmContext("Htk");
        Card updateCardTitle = getCard();
        updateCardTitle.setTitle(title);
        setCardOrmContext.update(updateCardTitle);
        setCardOrmContext.flush();
    }

    // 设置卡片封面图
    private void addCardCoverImg(String imgURI) {
        HiLog.info(HiLog_Label, HiLog_Format, "添加的图片URI：" + imgURI);
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext setCardOrmContext = helper.getOrmContext("Htk");
        Card updateCardImg = getCard();
        updateCardImg.setImgSrc(imgURI);
        setCardOrmContext.update(updateCardImg);
        setCardOrmContext.flush();
    }

    // 获取卡片对象
    private Card getCard() {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext getCardOrmContext = helper.getOrmContext("Htk");
        OrmPredicates query = getCardOrmContext.where(Card.class).equalTo("id", cardId);
        List<Card> cardList = getCardOrmContext.query(query);
        return cardList.get(0);
    }

    // 获取卡片内信息
    private String getCardInfo(String info) {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext getCardInfoOrmContext = helper.getOrmContext("Htk");
        OrmPredicates query = getCardInfoOrmContext.where(Card.class).equalTo("id", cardId);
        List<Card> cardList = getCardInfoOrmContext.query(query);
        Card getCardFromList = cardList.get(0);
        switch (info) {
            case "title":
                return getCardFromList.getTitle();
            case "describe":
                return getCardFromList.getDescribe();
            case "dueTime":
                return getCardFromList.getDueTime();
            case "imgSrc":
                return getCardFromList.getImgSrc();
            case "switch":
                return getCardFromList.getIsSwitch();
            case "reminderId":
                return String.valueOf(getCardFromList.getReminderId());
        }
        return "";
    }

    private void createDialog(String content) {
        CommonDialog dialog = new CommonDialog(getContext());
        Component container = LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_fail_warning, null, false);
        Text text = container.findComponentById(ResourceTable.Id_fail_content);
        text.setText(content);
        dialog.setContentCustomComponent(container);
        dialog.setSize(MATCH_CONTENT, MATCH_CONTENT);
        dialog.setButton(IDialog.BUTTON3, "已知晓", (iDialog, i) -> iDialog.destroy());
        dialog.show();
    }

    private void setCardImg(Uri uri, String type) {
        DataAbilityHelper helper = DataAbilityHelper.creator(getContext());
        ImageSource imageSource = null;
        try {
            FileDescriptor fd = helper.openFile(uri, "r");
            imageSource = ImageSource.create(fd, null);
            PixelMap pixelMap = imageSource.createPixelmap(null);
            Image image = new Image(getContext());
            image.setPixelMap(pixelMap);
            directionalLayout.removeAllComponents();
            directionalLayout.addComponent(image);
            if (type.equals("save")) {
                addCardCoverImg("tb-" + cover_src);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (imageSource != null) {
                imageSource.release();
            }
        }
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        if (resultData == null) {
            card_cover_setImg.setText("添加图片");
            return;
        }
        if (requestCode == imgRequestCode) {
            String chooseImgUri = resultData.getUriString();
            chooseImgId = chooseImgUri.substring(chooseImgUri.lastIndexOf('/') + 1);
            Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, chooseImgId);
            cover_src = uri.toString();
            setCardImg(uri, "save");
        }
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onBackground() {
        super.onBackground();
    }
}