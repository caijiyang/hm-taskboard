package com.felixcjy.hmtaskboard.slice;

import com.felixcjy.hmtaskboard.ResourceTable;
import com.felixcjy.hmtaskboard.pojo.PrivatePolicy;
import com.felixcjy.hmtaskboard.pojo.TUser;
import com.felixcjy.hmtaskboard.utils.JsonUtils;
import com.felixcjy.hmtaskboard.utils.PhoneNumberUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.WindowManager;
import ohos.data.DatabaseHelper;
import ohos.data.orm.OrmContext;
import ohos.data.orm.OrmPredicates;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.zson.ZSONObject;

import java.util.List;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_CONTENT;

public class RegisAbilitySlice extends AbilitySlice {

    private static final HiLogLabel HiLog_Label = new HiLogLabel(HiLog.LOG_APP, 0, "hmtb_RegisAbilitySlice");
    private static final String HiLog_Format = "%{public}s";

    // 用户输入的账号密码
    private String phone = "";
    private String password = "";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main_register);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(0xFFE4EFFF); // 设置状态栏颜色
        // 初始化组件
        initRegisAbilitySlice();
    }

    private void initRegisAbilitySlice() {
        // 注册按钮，跳转到登录页面。
        Button registerButton = findComponentById(ResourceTable.Id_register_button);
        TextField textField_phone = findComponentById(ResourceTable.Id_register_account_phone);
        TextField textField_password = findComponentById(ResourceTable.Id_register_password_pwd);
        Text show_password = findComponentById(ResourceTable.Id_register_show_password);
        show_password.setClickedListener(component1 -> {
            if (show_password.getText().equals("显示密码")) {
                textField_password.setTextInputType(InputAttribute.PATTERN_TEXT);
                show_password.setText("隐藏密码");
            } else {
                textField_password.setTextInputType(InputAttribute.PATTERN_PASSWORD);
                show_password.setText("显示密码");
            }
        });
        registerButton.setClickedListener(component -> {
            // 获取注册必要信息
            phone = textField_phone.getText();
            password = textField_password.getText();
            // 输入校验
            if (phone.equals("")) {
                String content;
                if (password.equals("")) {
                    content = "请输入账号和密码";
                } else {
                    content = "请输入手机号（账号）";
                }
                createDialog(content);
            } else if (password.equals("")) {
                String content = "请输入密码";
                createDialog(content);
            } else {
                if (PhoneNumberUtil.isPhone(phone)) {
                    // 正确的输入了。开始注册。
                    int database_res = operationDB(phone, password);
                    if (database_res == 1) {
                        // 注册成功
                        DirectionalLayout toastDialog_layout = (DirectionalLayout) LayoutScatter.getInstance(this)
                                .parse(ResourceTable.Layout_toast_dialog_grayback, null, false);
                        Text text_show = toastDialog_layout.findComponentById(ResourceTable.Id_msg_toast);
                        text_show.setText("注册成功");
                        new ToastDialog(getContext())
                                .setContentCustomComponent(toastDialog_layout)
                                .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                                .setAlignment(LayoutAlignment.CENTER)
                                .show();
                        // 跳转登录页面。
                        Intent intent1 = new Intent();
                        intent1.setParam("phone", phone);
                        intent1.setParam("password", password);
                        present(new LoginAbilitySlice(), intent1);
                        this.terminate();
                    } else if (database_res == 0) {
                        // 注册失败
                        String content = "注册失败，手机号已经被注册，请直接登录。";
                        createDialog(content);
                    }
                } else {
                    String content = "请输入正确的手机号！";
                    createDialog(content);
                }
            }
        });

        // 隐私政策 弹窗
        Text register_private = findComponentById(ResourceTable.Id_register_private);
        register_private.setClickedListener(component -> {
            String json = JsonUtils.getJsonFromPath("entry/resources/rawfile/privatePolicy.json", this);
            PrivatePolicy privatePolicy = ZSONObject.stringToClass(json, PrivatePolicy.class);
            CommonDialog dialog = new CommonDialog(getContext());
            Component container = LayoutScatter.getInstance(getContext())
                    .parse(ResourceTable.Layout_private_dialog, null, false);
            Text text = container.findComponentById(ResourceTable.Id_private_dialog_text);
            text.setText(privatePolicy.getPrivate_policy());
            dialog.setContentCustomComponent(container);
            dialog.setSize(MATCH_CONTENT, MATCH_CONTENT);
            dialog.setButton(IDialog.BUTTON3, "已知晓", (iDialog, i) -> iDialog.destroy());
            dialog.show();
        });
    }

    private int operationDB(String phone, String password) {
        // 打开数据库
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext getOrmContext_htk = helper.getOrmContext("Htk");
        // 查询手机号是否已经注册
        OrmPredicates query = getOrmContext_htk.where(TUser.class).equalTo("phone", phone);
        List<TUser> tUsers = getOrmContext_htk.query(query);
        if (tUsers.size() == 0) {
            // 未注册
            TUser tUser = new TUser();
            tUser.setPhone(phone);
            tUser.setEmail("");
            tUser.setHead("entry/resources/media/default_head.png");
            tUser.setNickName("默认昵称");
            tUser.setSign("这个人很懒，没有写任何签名~");
            tUser.setState(0);
            tUser.setPassword(password);
            getOrmContext_htk.insert(tUser);
            getOrmContext_htk.flush();
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * 创建一个弹窗
     *
     * @param content 弹窗内容。
     */
    private void createDialog(String content) {
        CommonDialog dialog = new CommonDialog(getContext());
        Component container = LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_fail_warning, null, false);
        Text text = container.findComponentById(ResourceTable.Id_fail_content);
        text.setText(content);
        dialog.setContentCustomComponent(container);
        dialog.setSize(MATCH_CONTENT, MATCH_CONTENT);
        dialog.setButton(IDialog.BUTTON3, "已知晓", (iDialog, i) -> iDialog.destroy());
        dialog.show();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
