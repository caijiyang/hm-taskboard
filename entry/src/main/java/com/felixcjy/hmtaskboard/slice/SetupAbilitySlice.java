package com.felixcjy.hmtaskboard.slice;

import com.felixcjy.hmtaskboard.MainAbility;
import com.felixcjy.hmtaskboard.ResourceTable;
import com.felixcjy.hmtaskboard.pojo.IsLogin;
import com.felixcjy.hmtaskboard.pojo.TUser;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.data.DatabaseHelper;
import ohos.data.orm.OrmContext;
import ohos.data.orm.OrmPredicates;
import ohos.data.rdb.ValuesBucket;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.util.List;

public class SetupAbilitySlice extends AbilitySlice {

    // 用户信息保存地。
    TUser tUser;
    // 圆形头像
    Image head_image;
    // 昵称
    Text nickName;
    // 签名
    Text sign;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_setup_ability);
        initSetupAbilitySlice();
    }

    private void initSetupAbilitySlice() {
        // 获取用户信息
        getUserInfo();
        // 圆形头像
        head_image = (Image) findComponentById(ResourceTable.Id_setup_img);
        head_image.setCornerRadius(1000f);
        if (!(tUser.getHead() == null) && !tUser.getHead().equals("")){
            char[] chars = tUser.getHead().toCharArray();
            if (chars[0] == 't'){
                String[] img1 = tUser.getHead().split("-");
                setUserHeadImg(Uri.parse(img1[1]));
            } else {
                head_image.setPixelMap(ResourceTable.Media_default_head);
            }
        } else {
            head_image.setPixelMap(ResourceTable.Media_default_head);
        }
        // 昵称
        nickName = (Text) findComponentById(ResourceTable.Id_setup_nickname);
        nickName.setText(tUser.getNickName());
        // 签名
        sign = (Text) findComponentById(ResourceTable.Id_setup_sign);
        sign.setText(tUser.getSign());

        // 个人信息 按钮
        DependentLayout info = (DependentLayout) findComponentById(ResourceTable.Id_setup_bg_info);
        info.setClickedListener(component -> {
            Intent info_intent = new Intent();
            Operation operation = new Intent.OperationBuilder().withDeviceId("")
                    .withAction("action.setup.info").build();
            info_intent.setOperation(operation);
            startAbility(info_intent);
        });

        // 关于应用 按钮
        DependentLayout about = (DependentLayout) findComponentById(ResourceTable.Id_setup_bg_about);
        about.setClickedListener(component -> {
            Intent things_intent = new Intent();
            Operation operation = new Intent.OperationBuilder().withDeviceId("")
                    .withAction("action.setup.about").build();
            things_intent.setOperation(operation);
            startAbility(things_intent);
        });

        // 隐私中心 按钮
        DependentLayout privacy = (DependentLayout) findComponentById(ResourceTable.Id_setup_bg_privacy);
        privacy.setClickedListener(component -> {
            Intent things_intent = new Intent();
            Operation operation = new Intent.OperationBuilder().withDeviceId("")
                    .withAction("action.setup.privacy").build();
            things_intent.setOperation(operation);
            startAbility(things_intent);
        });

        // 退出登录 按钮
        DependentLayout exit = (DependentLayout) findComponentById(ResourceTable.Id_setup_bg_exit);
        exit.setClickedListener(component -> {
            operationDB();
            Intent intent = new Intent();
            Operation operation = new Intent.OperationBuilder().withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName(MainAbility.class.getName())
                    .build();
            intent.setOperation(operation);
            startAbility(intent);
            // 关闭此页面
            this.terminateAbility();
        });
    }

    private void setUserHeadImg(Uri uri) {
        DataAbilityHelper helper = DataAbilityHelper.creator(getContext());
        ImageSource imageSource = null;
        try {
            FileDescriptor fd = helper.openFile(uri, "r");
            imageSource = ImageSource.create(fd, null);
            PixelMap pixelMap = imageSource.createPixelmap(null);
            head_image.setPixelMap(pixelMap);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (imageSource != null) {
                imageSource.release();
            }
        }
    }

    private void getUserInfo() {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext getUserInfo_htk = helper.getOrmContext("Htk");
        OrmPredicates query = getUserInfo_htk.where(TUser.class).equalTo("state", 1);
        List<TUser> tUsers = getUserInfo_htk.query(query);
        tUser = tUsers.get(0);
    }

    private void operationDB() {
        // 打开数据库
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext getOrmContext_htk = helper.getOrmContext("Htk");
        // 设置数据集
        ValuesBucket valuesBucket = new ValuesBucket();
        valuesBucket.putInteger("id", 1);
        valuesBucket.putInteger("state", 0);
        // 持久化到数据库中
        OrmPredicates update = getOrmContext_htk.where(IsLogin.class).equalTo("id", 1);
        getOrmContext_htk.update(update, valuesBucket);
        // 更新人物登录信息
        tUser.setState(0);
        getOrmContext_htk.update(tUser);
        getOrmContext_htk.flush();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onActive() {
        super.onActive();
        // 更新人物信息
        getUserInfo();
        nickName.setText(tUser.getNickName());
        sign.setText(tUser.getSign());
        if (!(tUser.getHead() == null) && !tUser.getHead().equals("")){
            char[] chars = tUser.getHead().toCharArray();
            if (chars[0] == 't'){
                String[] img1 = tUser.getHead().split("-");
                setUserHeadImg(Uri.parse(img1[1]));
            } else {
                head_image.setPixelMap(ResourceTable.Media_default_head);
            }
        } else {
            head_image.setPixelMap(ResourceTable.Media_default_head);
        }
    }
}
