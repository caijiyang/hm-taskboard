package com.felixcjy.hmtaskboard.slice;

import com.felixcjy.hmtaskboard.HomePageAbility;
import com.felixcjy.hmtaskboard.ResourceTable;
import com.felixcjy.hmtaskboard.pojo.IsLogin;
import com.felixcjy.hmtaskboard.pojo.PrivatePolicy;
import com.felixcjy.hmtaskboard.pojo.TUser;
import com.felixcjy.hmtaskboard.utils.JsonUtils;
import com.felixcjy.hmtaskboard.utils.PhoneNumberUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.WindowManager;
import ohos.data.DatabaseHelper;
import ohos.data.orm.OrmContext;
import ohos.data.orm.OrmPredicates;
import ohos.data.rdb.ValuesBucket;
import ohos.utils.zson.ZSONObject;

import java.util.List;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_CONTENT;

/**
 * 登录页面
 */

public class LoginAbilitySlice extends AbilitySlice {

    private String phone = "";
    private String password = "";


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main_login);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(0xFFE4EFFF); // 设置状态栏颜色
        // 注册跳转过来的时候，获取账号密码并填充到页面中。
        IntentParams intentParams = intent.getParams();
        if (intentParams != null) {
            phone = (String) intentParams.getParam("phone");
            password = (String) intentParams.getParam("password");
            TextField phone_page = findComponentById(ResourceTable.Id_login_account);
            TextField password_page = findComponentById(ResourceTable.Id_login_password);
            phone_page.setText(phone);
            password_page.setText(password);
        }
        // 初始化页面组件。
        initLoginAbilitySlice();
    }

    private void initLoginAbilitySlice() {
        TextField phone1 = findComponentById(ResourceTable.Id_login_account);
        TextField password1 = findComponentById(ResourceTable.Id_login_password);
        Text show_password = findComponentById(ResourceTable.Id_login_show_password);
        show_password.setClickedListener(component1 -> {
            if (show_password.getText().equals("显示密码")) {
                password1.setTextInputType(InputAttribute.PATTERN_TEXT);
                show_password.setText("隐藏密码");
            } else {
                password1.setTextInputType(InputAttribute.PATTERN_PASSWORD);
                show_password.setText("显示密码");
            }
        });
        // 隐私政策 弹窗
        Text login_private = findComponentById(ResourceTable.Id_login_private);
        login_private.setClickedListener(component -> {
            String json = JsonUtils.getJsonFromPath("entry/resources/rawfile/privatePolicy.json", this);
            PrivatePolicy privatePolicy = ZSONObject.stringToClass(json, PrivatePolicy.class);
            CommonDialog dialog = new CommonDialog(getContext());
            Component container = LayoutScatter.getInstance(getContext())
                    .parse(ResourceTable.Layout_private_dialog, null, false);
            Text text = container.findComponentById(ResourceTable.Id_private_dialog_text);
            text.setText(privatePolicy.getPrivate_policy());
            dialog.setContentCustomComponent(container);
            dialog.setSize(MATCH_CONTENT, MATCH_CONTENT);
            dialog.setButton(IDialog.BUTTON3, "已知晓", (iDialog, i) -> iDialog.destroy());
            dialog.show();
        });

        // 登录按钮
        Button button = findComponentById(ResourceTable.Id_LoginButton);
        button.setClickedListener(component -> {
            phone = phone1.getText();
            password = password1.getText();
            // 输入校验
            if (phone.equals("")) {
                String content;
                if (password.equals("")) {
                    content = "请输入账号和密码";
                } else {
                    content = "请输入手机号（账号）";
                }
                createDialog(content);
            } else if (password.equals("")) {
                String content = "请输入密码";
                createDialog(content);
            } else {
                if (PhoneNumberUtil.isPhone(phone)) {
                    // 数据库操作.1 为信息校验成功,2 密码错误，3 用户未注册。
                    int database_res = operationDB();
                    if (database_res == 1) {
                        // 信息正确，可进入主界面。清除输入的信息。
                        password1.setText("");
                        phone1.setText("");
                        DirectionalLayout toastDialog_layout = (DirectionalLayout) LayoutScatter.getInstance(this)
                                .parse(ResourceTable.Layout_toast_dialog_grayback, null, false);
                        Text text_show = toastDialog_layout.findComponentById(ResourceTable.Id_msg_toast);
                        text_show.setText("登录成功");
                        new ToastDialog(getContext())
                                .setContentCustomComponent(toastDialog_layout)
                                .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                                .setAlignment(LayoutAlignment.CENTER)
                                .show();
                        // 跳转到程序主界面
                        Intent intent1 = new Intent();
                        Operation operation = new Intent.OperationBuilder().withDeviceId("")
                                .withBundleName(getBundleName())
                                .withAbilityName(HomePageAbility.class.getName())
                                .build();
                        intent1.setOperation(operation);
                        startAbility(intent1);
//                    this.terminate();
                        this.terminateAbility();
                    } else if (database_res == 2) {
                        String content = "密码错误，请重新输入";
                        createDialog(content);
                        // 清除密码框
                        Text text1 = findComponentById(ResourceTable.Id_login_password);
                        text1.setText("");
                    } else if (database_res == 3) {
                        String content = "该账户未注册，请先前往注册页面。";
                        createDialog(content);
                    }
                } else {
                    String content = "请输入正确的手机号！";
                    createDialog(content);
                }
            }
        });

        // 跳转到注册页面
        Button button1 = findComponentById(ResourceTable.Id_loginToRegister);
        button1.setClickedListener(component -> {
            Intent intent1 = new Intent();
            present(new RegisAbilitySlice(), intent1);
        });

        Text forgetPassword = findComponentById(ResourceTable.Id_forgetPassword);
        forgetPassword.setClickedListener(component -> {
            Intent intent1 = new Intent();
            present(new ForgetPasswordAbilitySlice(), intent1);
        });

    }

    // 登录按钮设置数据库
    private int operationDB() {
        // 打开数据库
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext getOrmContext_htk = helper.getOrmContext("Htk");
        // 查询登录的人是否存在
        OrmPredicates query = getOrmContext_htk.where(TUser.class).equalTo("phone", phone);
        List<TUser> tUsers = getOrmContext_htk.query(query);
        if (tUsers.size() == 0) {
            // 用户未注册
            return 3;
        } else {
            // 账号已存在，再比对密码。
            OrmPredicates query1 = getOrmContext_htk.where(TUser.class).equalTo("phone", phone).and()
                    .equalTo("password", password);
            List<TUser> tUsers1 = getOrmContext_htk.query(query1);
            if (tUsers1.size() == 0) {
                // 密码错误
                return 2;
            } else {
                // 更新状态信息
                TUser tUser = tUsers1.get(0);
                tUser.setState(1);
                getOrmContext_htk.update(tUser);
                getOrmContext_htk.flush();
                // 登录成功，设置数据集（登录状态信息）
                ValuesBucket valuesBucket = new ValuesBucket();
                valuesBucket.putInteger("id", 1);
                valuesBucket.putInteger("state", 1);
                // 持久化到数据库中
                OrmPredicates update = getOrmContext_htk.where(IsLogin.class).equalTo("id", 1);
                getOrmContext_htk.update(update, valuesBucket);
                return 1;
            }
        }
    }

    /**
     * 创建一个弹窗
     *
     * @param content 弹窗内容。
     */
    private void createDialog(String content) {
        CommonDialog dialog = new CommonDialog(getContext());
        Component container = LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_fail_warning, null, false);
        Text text = container.findComponentById(ResourceTable.Id_fail_content);
        text.setText(content);
        dialog.setContentCustomComponent(container);
        dialog.setSize(MATCH_CONTENT, MATCH_CONTENT);
        dialog.setButton(IDialog.BUTTON3, "已知晓", (iDialog, i) -> iDialog.destroy());
        dialog.show();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
