package com.felixcjy.hmtaskboard.slice;

import com.felixcjy.hmtaskboard.ResourceTable;
import com.felixcjy.hmtaskboard.TaskBoardAbility;
import com.felixcjy.hmtaskboard.pojo.TBoard;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.data.DatabaseHelper;
import ohos.data.orm.OrmContext;
import ohos.data.orm.OrmPredicates;

import java.util.List;

/**
 * @author 蔡济阳（FelixCJY / FelixCai）
 * @date 2022/05/14 12:53
 **/

public class SearchAbilitySlice extends AbilitySlice {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_search);
        initSearchAbilitySlice();
    }

    private void initSearchAbilitySlice(){
        Image back = findComponentById(ResourceTable.Id_search_page_title_back);
        back.setClickedListener(component -> onBackPressed());

        TextField textField = findComponentById(ResourceTable.Id_search_tb_name);
        Text search_text = findComponentById(ResourceTable.Id_search_text);
        DirectionalLayout directionalLayout = findComponentById(ResourceTable.Id_search_res_content);

        Button search_btn = findComponentById(ResourceTable.Id_search_btn);
        search_btn.setClickedListener(component -> {
            String tb_name = textField.getText();
            List<TBoard> list_tb = findTaskBoardFromDB(tb_name);
            if (list_tb != null && !(list_tb.size() == 0)) {
                search_text.setText("搜索结果");
                directionalLayout.removeAllComponents();
                for (int i = 0; i < list_tb.size(); i++) {
                    TBoard tBoard = list_tb.get(i);
                    Component tb = LayoutScatter.getInstance(this)
                            .parse(ResourceTable.Layout_home_item_taskboard, null, false);
                    Text title = tb.findComponentById(ResourceTable.Id_id_item_tb_title);
                    title.setText(tBoard.getTitle());
                    Text common = tb.findComponentById(ResourceTable.Id_id_item_tb_describe);
                    String common_tb = tBoard.getDescribe();
                    if (common_tb != null && !common_tb.equals("")) {
                        common.setText(common_tb);
                    } else {
                        common.setText("暂无描述");
                    }
                    ShapeElement element = new ShapeElement();
                    element.setRgbColor(getRgbColor(tBoard.getColor()));
                    Component color = tb.findComponentById(ResourceTable.Id_id_item_tb_color);
                    color.setBackground(element);
                    tb.setClickedListener(component1 -> {
                        Intent intent_board = new Intent();
                        Operation operation = new Intent.OperationBuilder().withDeviceId("")
                                .withBundleName(getBundleName())
                                .withAbilityName(TaskBoardAbility.class.getName())
                                .build();
                        intent_board.setOperation(operation);
                        intent_board.setParam("tbId", tBoard.getId() + "");
                        startAbility(intent_board);
                    });
                    directionalLayout.addComponent(tb);
                }
            } else {
                search_text.setText("无结果");
                directionalLayout.removeAllComponents();
            }
        });
    }

    private List<TBoard> findTaskBoardFromDB(String tb_name) {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext getOrmContext_htk = helper.getOrmContext("Htk");
        OrmPredicates query_tb = getOrmContext_htk.where(TBoard.class).equalTo("title",tb_name);
        return getOrmContext_htk.query(query_tb);
    }

    private RgbColor getRgbColor(Integer color) {
        if (color == 0) {
            return new RgbColor(207,197,226);
        } else if (color == 1) {
            return new RgbColor(255,184,179);
        } else if (color == 2) {
            return new RgbColor(181,204,141);
        } else if (color == 3) {
            return new RgbColor(166,186,221);
        }
        return new RgbColor(241,215,159);
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onBackground() {
        super.onBackground();
    }
}
