package com.felixcjy.hmtaskboard.slice;

import com.felixcjy.hmtaskboard.ResourceTable;
import com.felixcjy.hmtaskboard.pojo.TUser;
import com.felixcjy.hmtaskboard.utils.PhoneNumberUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ToastDialog;
import ohos.data.DatabaseHelper;
import ohos.data.orm.OrmContext;
import ohos.data.orm.OrmPredicates;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.IntentConstants;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.util.List;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_CONTENT;

public class SetUpInnerInfoAbilitySlice extends AbilitySlice {

    private static final HiLogLabel HiLog_Label = new HiLogLabel(HiLog.LOG_APP, 0, "hmtb_SetUpInnerInfoAbilitySlice");
    private static final String HiLog_Format = "%{public}s";

    int imgRequestCode = 12345;

    Image user_head; // 头像
    Text user_name; // 昵称
    Text user_sign; // 个性签名
    Text user_phone; // 电话号码
    Text user_email; // 邮箱
    Text user_id; // 用户ID

    // 修改按钮
    Image head_icon;
    String chooseImgId;
    String cover_src = "";

    Image name_icon;
    Image sign_icon;
    Image phone_icon;
    Image email_icon;
    Image info_back;

    // 修改密码
    Text change_password;

    TUser tUser;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_set_up_inner_info);
        initSetUpInnerInfoAbilitySlice();
    }

    private void initSetUpInnerInfoAbilitySlice() {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext getUserOrmContext = helper.getOrmContext("Htk");
        OrmPredicates query = getUserOrmContext.where(TUser.class).equalTo("state", 1);
        List<TUser> tUsers = getUserOrmContext.query(query);
        tUser = tUsers.get(0);
        // 初始化组件。设置头像圆角。
        user_head = findComponentById(ResourceTable.Id_info_head);
        user_head.setCornerRadius(100f);
        if (!(tUser.getHead() == null) && !tUser.getHead().equals("")) {
            char[] chars = tUser.getHead().toCharArray();
            HiLog.info(HiLog_Label, HiLog_Format, "首字母是：" + chars[0]);
            if (chars[0] == 't') {
                String[] img1 = tUser.getHead().split("-");
                HiLog.info(HiLog_Label, HiLog_Format, "有头像");
                setUserHeadImg(Uri.parse(img1[1]), "read");
            } else {
                HiLog.info(HiLog_Label, HiLog_Format, "默认头像");
                user_head.setPixelMap(ResourceTable.Media_default_head);
            }
        } else {
            HiLog.info(HiLog_Label, HiLog_Format, "没头像");
            // 设置默认头像
            user_head.setPixelMap(ResourceTable.Media_default_head);
        }
        user_name = findComponentById(ResourceTable.Id_info_name_text);
        user_sign = findComponentById(ResourceTable.Id_info_sign_text);
        user_phone = findComponentById(ResourceTable.Id_info_phone_text);
        user_email = findComponentById(ResourceTable.Id_info_email_text);
        user_id = findComponentById(ResourceTable.Id_info_uid_text);
        head_icon = findComponentById(ResourceTable.Id_info_head_icon);
        name_icon = findComponentById(ResourceTable.Id_info_name_icon);
        sign_icon = findComponentById(ResourceTable.Id_info_sign_icon);
        phone_icon = findComponentById(ResourceTable.Id_info_phone_icon);
        email_icon = findComponentById(ResourceTable.Id_info_email_icon);
        info_back = findComponentById(ResourceTable.Id_info_back);
        change_password = findComponentById(ResourceTable.Id_info_change_password_text);
        operationDB("read", "");
        info_back.setClickedListener(component -> onBackPressed());
        head_icon.setClickedListener(component -> {
            Intent intent = new Intent();
            Operation opt = new Intent.OperationBuilder().withAction(IntentConstants.ACTION_CHOOSE).build();
            intent.setOperation(opt);
            intent.addFlags(Intent.FLAG_NOT_OHOS_COMPONENT);
            intent.setType("image/*");
            startAbilityForResult(intent, imgRequestCode);
        });
        name_icon.setClickedListener(component -> createOperationDialog("修改昵称", tUser.getNickName()));
        sign_icon.setClickedListener(component -> createOperationDialog("修改签名", tUser.getSign()));
        phone_icon.setClickedListener(component -> createOperationDialog("修改手机号", tUser.getPhone()));
        email_icon.setClickedListener(component -> createOperationDialog("修改邮箱", tUser.getEmail()));
        change_password.setClickedListener(component -> changePasswordDialog());
    }

    private void changePasswordDialog() {
        CommonDialog dialog = new CommonDialog(getContext());
        Component container = LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_change_password, null, false);
        Button changePasswordBtn = container.findComponentById(ResourceTable.Id_change_password_btn);
        Text u_phone = container.findComponentById(ResourceTable.Id_change_password_phone_text);
        u_phone.setText(tUser.getPhone());
        Text u_id = container.findComponentById(ResourceTable.Id_change_password_uid_text);
        u_id.setText(String.valueOf(tUser.getId()));
        TextField old_pwd = container.findComponentById(ResourceTable.Id_chenge_password_old_text);
        TextField new_pwd = container.findComponentById(ResourceTable.Id_chenge_password_new_text);
        TextField new2_pwd = container.findComponentById(ResourceTable.Id_chenge_password_new2_text);
        changePasswordBtn.setClickedListener(component -> {
            String new_password = new_pwd.getText();
            String new_password2 = new2_pwd.getText();
            String old_password = old_pwd.getText();
            HiLog.info(HiLog_Label, HiLog_Format, "旧密码：" + old_password + " 新密码：" + new_password + " 新密码2：" + new_password2);
            // 新密码两次是否输入相同
            if (new_password.equals("")) {
                createDialog("请输入新密码");
            } else if (new_password2.equals("")) {
                createDialog("请再次输入新密码");
            } else if (!new_password.equals(new_password2)) {
                createDialog("两次输入的密码不相同");
            } else if (old_password.equals("")) {
                createDialog("请输入原密码");
            } else if (!old_password.equals(tUser.getPassword())) {
                createDialog("原密码输入错误");
            } else {
                DirectionalLayout toastDialog_layout = (DirectionalLayout) LayoutScatter.getInstance(this)
                        .parse(ResourceTable.Layout_toast_dialog_grayback, null, false);
                Text text_show = toastDialog_layout.findComponentById(ResourceTable.Id_msg_toast);
                text_show.setText("密码修改成功");
                new ToastDialog(getContext())
                        .setContentCustomComponent(toastDialog_layout)
                        .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                        .setAlignment(LayoutAlignment.BOTTOM)
                        .show();
                operationDB("changePassword", new_password);
                dialog.destroy();
            }
        });
        dialog.setContentCustomComponent(container);
        dialog.setSize(MATCH_CONTENT, MATCH_CONTENT);
        dialog.setButton(IDialog.BUTTON3, "取消", (iDialog, i) -> iDialog.destroy());
        dialog.show();
    }

    private void createOperationDialog(String title, String content) {
        CommonDialog dialog = new CommonDialog(getContext());
        Component container = LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_change_info, null, false);
        Text component_title = container.findComponentById(ResourceTable.Id_change_info_title);
        TextField component_content = container.findComponentById(ResourceTable.Id_change_info_input);
        Button component_btn = container.findComponentById(ResourceTable.Id_change_info_btn);

        component_title.setText(title);
        component_content.setHint(content);
        component_btn.setClickedListener(component -> {
            switch (title) {
                case "修改昵称":
                    operationDB("changeName", component_content.getText());
                    break;
                case "修改签名":
                    operationDB("changeSign", component_content.getText());
                    break;
                case "修改手机号":
                    if (PhoneNumberUtil.isPhone(component_content.getText())) {
                        operationDB("changePhone", component_content.getText());
                        break;
                    } else {
                        DirectionalLayout toastDialog_layout = (DirectionalLayout) LayoutScatter.getInstance(this)
                                .parse(ResourceTable.Layout_toast_dialog_grayback, null, false);
                        Text text_show = toastDialog_layout.findComponentById(ResourceTable.Id_msg_toast);
                        text_show.setText("请输入正确的手机号");
                        new ToastDialog(getContext())
                                .setContentCustomComponent(toastDialog_layout)
                                .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                                .setAlignment(LayoutAlignment.CENTER)
                                .show();
                    }
                case "修改邮箱":
                    operationDB("changeEmail", component_content.getText());
                    break;
            }
            dialog.destroy();
        });
        dialog.setContentCustomComponent(container);
        dialog.setSize(MATCH_CONTENT, MATCH_CONTENT);
        dialog.setButton(IDialog.BUTTON3, "取消", (iDialog, i) -> iDialog.destroy());
        dialog.show();
    }


    private void createDialog(String content) {
        CommonDialog dialog = new CommonDialog(getContext());
        Component container = LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_fail_warning, null, false);
        Text text = container.findComponentById(ResourceTable.Id_fail_content);
        text.setText(content);
        dialog.setContentCustomComponent(container);
        dialog.setSize(MATCH_CONTENT, MATCH_CONTENT);
        dialog.setButton(IDialog.BUTTON3, "已知晓", (iDialog, i) -> iDialog.destroy());
        dialog.show();
    }

    /**
     * 操作数据库。
     *
     * @param operation add,delete,update,read
     */
    private void operationDB(String operation, String content) {
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext htk_OrmContext = helper.getOrmContext("Htk");
        // 查询出已登录的人员信息。
        switch (operation) {
            case "read":
                user_name.setText(tUser.getNickName());
                user_sign.setText(tUser.getSign());
                user_phone.setText(tUser.getPhone());
                user_email.setText(tUser.getEmail());
                user_id.setText(tUser.getId() + "");
                break;
            case "changeName":
                tUser.setNickName(content);
                user_name.setText(content);
                htk_OrmContext.update(tUser);
                htk_OrmContext.flush();
                break;
            case "changeSign":
                tUser.setSign(content);
                user_sign.setText(content);
                htk_OrmContext.update(tUser);
                htk_OrmContext.flush();
                break;
            case "changePhone":
                tUser.setPhone(content);
                user_phone.setText(content);
                htk_OrmContext.update(tUser);
                htk_OrmContext.flush();
                break;
            case "changeEmail":
                tUser.setEmail(content);
                user_email.setText(content);
                htk_OrmContext.update(tUser);
                htk_OrmContext.flush();
                break;
            case "changePassword":
                tUser.setPassword(content);
                htk_OrmContext.update(tUser);
                htk_OrmContext.flush();
                break;
        }
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        if (resultData == null) {
            return;
        }
        if (requestCode == imgRequestCode) {
            String chooseImgUri = resultData.getUriString();
            chooseImgId = chooseImgUri.substring(chooseImgUri.lastIndexOf('/') + 1);
            Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, chooseImgId);
            cover_src = uri.toString();
            setUserHeadImg(uri, "save");
        }
    }

    private void setUserHeadImg(Uri uri, String type) {
        DataAbilityHelper helper = DataAbilityHelper.creator(getContext());
        ImageSource imageSource = null;
        try {
            FileDescriptor fd = helper.openFile(uri, "r");
            imageSource = ImageSource.create(fd, null);
            PixelMap pixelMap = imageSource.createPixelmap(null);
            user_head.setPixelMap(pixelMap);
            if (type.equals("save")) {
                addUserHeadImg("tb-" + cover_src);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (imageSource != null) {
                imageSource.release();
            }
        }
    }

    // 设置卡片封面图
    private void addUserHeadImg(String imgURI) {
        HiLog.info(HiLog_Label, HiLog_Format, "添加的头像URI：" + imgURI);
        DatabaseHelper helper = new DatabaseHelper(this);
        OrmContext setCardOrmContext = helper.getOrmContext("Htk");
        tUser.setHead(imgURI);
        setCardOrmContext.update(tUser);
        setCardOrmContext.flush();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}