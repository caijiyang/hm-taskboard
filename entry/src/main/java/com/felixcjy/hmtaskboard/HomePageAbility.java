package com.felixcjy.hmtaskboard;

import com.felixcjy.hmtaskboard.slice.*;
import com.felixcjy.hmtaskboard.utils.DisplayUtils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySliceAnimator;
import ohos.aafwk.content.Intent;
import ohos.bundle.IBundleManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.security.SystemPermission;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class HomePageAbility extends Ability {

    private static final HiLogLabel hiloglabel = new HiLogLabel(HiLog.LOG_APP, 0, "hmtb_HomePageAbility");
    private static final String log_format = "%{public}s";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(HomePageAbilitySlice.class.getName());
        requestPermissions();
        // 切换动画，水平右移，从（0，0）原点，屏幕有上角，水平移动到（1080，0）屏幕右上角，width 为屏幕宽度
        int width = DisplayUtils.getDisplayWidthInPx(getContext());
        setAbilitySliceAnimator(new AbilitySliceAnimator(-width, 0, 0, 0)
                .setDuration(500)
                .setRepeatCount(0));
        // 添加路由-- 主界面到设置页面。
        addActionRoute("action.homeToSetup", SetupAbilitySlice.class.getName());
        // 个人信息 页面
        addActionRoute("action.setup.info", SetUpInnerInfoAbilitySlice.class.getName());
        // 关于应用 页面
        addActionRoute("action.setup.about", SetUpInnerAboutAbilitySlice.class.getName());
        // 隐私中心 页面
        addActionRoute("action.setup.privacy", SetUpInnerPrivacyAbilitySlice.class.getName());
        // 搜索
        addActionRoute("action.home.search",SearchAbilitySlice.class.getName());
    }

    private void requestPermissions() {
        String[] permissions = {
                SystemPermission.WRITE_MEDIA,
                SystemPermission.READ_MEDIA,
                SystemPermission.DISTRIBUTED_DATASYNC
        };
        List<String> permissionFiltered = Arrays.stream(permissions)
                .filter(permission -> verifySelfPermission(permission) != IBundleManager.PERMISSION_GRANTED)
                .collect(Collectors.toList());
        requestPermissionsFromUser(permissionFiltered.toArray(new String[permissionFiltered.size()]), 0);
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions
            , int[] grantResults) {
        if (permissions == null || permissions.length == 0
                || grantResults == null || grantResults.length == 0) {
            return;
        }
        for (int grantResult : grantResults) {
            if (grantResult != IBundleManager.PERMISSION_GRANTED) {
                terminateAbility();
                break;
            }
        }
    }
}
