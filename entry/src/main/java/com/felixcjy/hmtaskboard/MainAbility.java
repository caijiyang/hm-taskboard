package com.felixcjy.hmtaskboard;

import com.felixcjy.hmtaskboard.slice.ForgetPasswordAbilitySlice;
import com.felixcjy.hmtaskboard.slice.LoginAbilitySlice;
import com.felixcjy.hmtaskboard.slice.MainAbilitySlice;
import com.felixcjy.hmtaskboard.slice.RegisAbilitySlice;
import com.felixcjy.hmtaskboard.utils.PermissionBridge;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.security.SystemPermission;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static ohos.bundle.IBundleManager.PERMISSION_GRANTED;

public class MainAbility extends Ability {
    private static final int PERMISSION_REQUEST_CODE = 1;
    public static final int EVENT_PERMISSION_GRANTED = 0x0000023;
    public static final int EVENT_PERMISSION_DENIED = 0x0000024;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        // 添加路由
        addActionRoute("action.login", LoginAbilitySlice.class.getName());
        addActionRoute("action.register", RegisAbilitySlice.class.getName());
        addActionRoute("action.forget.password", ForgetPasswordAbilitySlice.class.getName());
        super.setMainRoute(MainAbilitySlice.class.getName());
//        requestPermissions();
    }

    private void requestPermissions() {
        List<String> permissions =
                new LinkedList<String>(
                        Arrays.asList(
                                SystemPermission.WRITE_MEDIA,
                                SystemPermission.READ_MEDIA,
                                SystemPermission.CAMERA,
                                SystemPermission.DISTRIBUTED_DATASYNC,
                                SystemPermission.MICROPHONE));
        permissions.removeIf(
                permission ->
                        verifySelfPermission(permission) == PERMISSION_GRANTED ||
                                !canRequestPermission(permission));

        if (!permissions.isEmpty()) {
            requestPermissionsFromUser(permissions.toArray(new String[permissions.size()]),
                    PERMISSION_REQUEST_CODE);
        } else {
            PermissionBridge.getHandler().sendEvent(EVENT_PERMISSION_GRANTED);
        }
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions,
                                                   int[] grantResults) {
        if (requestCode != PERMISSION_REQUEST_CODE) {
            return;
        }
        for (int grantResult : grantResults) {
            if (grantResult != PERMISSION_GRANTED) {
                PermissionBridge.getHandler().sendEvent(EVENT_PERMISSION_DENIED);
                terminateAbility();
                return;
            }
        }
        PermissionBridge.getHandler().sendEvent(EVENT_PERMISSION_GRANTED);
    }
}
