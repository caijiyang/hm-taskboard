package com.felixcjy.hmtaskboard;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.rpc.IRemoteObject;
import ohos.rpc.RemoteException;

public class ShareServiceAbility extends Ability {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD001100, "Demo");
    private static final HiLogLabel HiLog_Label = new HiLogLabel(HiLog.LOG_APP, 0, "hmtb_ShareServiceAbility");
    private static final String HiLog_Format = "%{public}s";

    @Override
    public void onStart(Intent intent) {
        HiLog.error(LABEL_LOG, "ShareServiceAbility::onStart");
        super.onStart(intent);
    }

    @Override
    public void onBackground() {
        super.onBackground();
        HiLog.info(LABEL_LOG, "ShareServiceAbility::onBackground");
    }

    @Override
    public void onStop() {
        super.onStop();
        HiLog.info(LABEL_LOG, "ShareServiceAbility::onStop");
    }

    @Override
    public void onCommand(Intent intent, boolean restart, int startId) {
    }

    @Override
    public IRemoteObject onConnect(Intent intent) {
        // 一般传入全类名
        return new MyBinder("com.felixcjy.hmtaskboard.IShareIdlInterface");
//        return null;
    }

    class MyBinder extends ShareIdlInterfaceStub {

        public MyBinder(String descriptor) {
            super(descriptor);
        }

        @Override
        public void serviceMethod1(int _anInt) throws RemoteException {

        }

        @Override
        public void trans(int _pageId, int _tBoardId, int _cardId) throws RemoteException {
            HiLog.info(HiLog_Label,HiLog_Format,"开始详情页调用");
            // 实现详情页面的调用
            Intent intent = new Intent();
            intent.setParam("pageId",_pageId);
            intent.setParam("tBoardId",_tBoardId);
            intent.setParam("cardId",_cardId);
            Operation operation= new Intent.OperationBuilder()
                    .withBundleName(getBundleName())
                    .withAbilityName(CardDetailAbility.class.getName())
                    .build();
            intent.setOperation(operation);
            startAbility(intent);
        }
    }

    @Override
    public void onDisconnect(Intent intent) {
    }
}