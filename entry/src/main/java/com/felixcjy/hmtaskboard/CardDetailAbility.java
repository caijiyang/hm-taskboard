package com.felixcjy.hmtaskboard;

import com.felixcjy.hmtaskboard.slice.CardDetailAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class CardDetailAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(CardDetailAbilitySlice.class.getName());
    }
}
