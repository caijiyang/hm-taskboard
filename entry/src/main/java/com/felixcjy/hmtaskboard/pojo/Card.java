package com.felixcjy.hmtaskboard.pojo;

import ohos.data.orm.OrmObject;
import ohos.data.orm.annotation.Entity;
import ohos.data.orm.annotation.PrimaryKey;

@Entity(tableName = "card")
public class Card extends OrmObject {

    @PrimaryKey(autoGenerate = true)
    Integer id;
    Integer tBoardId;
    Integer pageId;
    String title;
    String describe;
    String dueTime;
    String isSwitch;
    String imgSrc;
    Integer reminderId;

    public Card() {
    }

    public Card(Integer tBoardId, Integer pageId, String title) {
        this.tBoardId = tBoardId;
        this.pageId = pageId;
        this.title = title;
    }

    public Card(Integer tBoardId, Integer pageId, String title, String describe, String imgSrc) {
        this.tBoardId = tBoardId;
        this.pageId = pageId;
        this.title = title;
        this.describe = describe;
        this.imgSrc = imgSrc;
    }

    public Card(Integer id, Integer tBoardId, Integer pageId, String title, String describe, String imgSrc) {
        this.id = id;
        this.tBoardId = tBoardId;
        this.pageId = pageId;
        this.title = title;
        this.describe = describe;
        this.imgSrc = imgSrc;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer gettBoardId() {
        return tBoardId;
    }

    public void settBoardId(Integer tBoardId) {
        this.tBoardId = tBoardId;
    }

    public Integer getPageId() {
        return pageId;
    }

    public void setPageId(Integer pageId) {
        this.pageId = pageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }

    public String getDueTime() {
        return dueTime;
    }

    public void setDueTime(String dueTime) {
        this.dueTime = dueTime;
    }

    public String getIsSwitch() {
        return isSwitch;
    }

    public void setIsSwitch(String isSwitch) {
        this.isSwitch = isSwitch;
    }

    public Integer getReminderId() {
        return reminderId;
    }

    public void setReminderId(Integer reminderId) {
        this.reminderId = reminderId;
    }
}
