package com.felixcjy.hmtaskboard.pojo;

public class PrivatePolicy {
    String private_policy;

    public PrivatePolicy() {
    }

    public PrivatePolicy(String private_policy) {
        this.private_policy = private_policy;
    }

    public String getPrivate_policy() {
        return private_policy;
    }

    public void setPrivate_policy(String private_policy) {
        this.private_policy = private_policy;
    }
}
