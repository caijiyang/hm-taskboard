package com.felixcjy.hmtaskboard.pojo;

import ohos.data.orm.OrmObject;
import ohos.data.orm.annotation.Entity;
import ohos.data.orm.annotation.PrimaryKey;

/**
 * @author 蔡济阳（FelixCJY / FelixCai）
 * @date 2022/04/29 14:33
 **/

@Entity(tableName = "label")
public class Label extends OrmObject {

    @PrimaryKey(autoGenerate = true)
    Integer id;
    String name;

    public Label() {
    }

    public Label(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
