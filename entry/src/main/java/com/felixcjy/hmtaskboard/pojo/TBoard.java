package com.felixcjy.hmtaskboard.pojo;

import ohos.data.orm.OrmObject;
import ohos.data.orm.annotation.Entity;
import ohos.data.orm.annotation.PrimaryKey;
/**
 * @author 蔡济阳（FelixCJY / FelixCai）
 * @date 2022/04/16 15:31
 **/

@Entity(tableName = "tBoard")
public class TBoard extends OrmObject {

    @PrimaryKey(autoGenerate = true)
    Integer id;
    Integer color;
    String title;
    String describe;
    Integer userId;

    public TBoard() {
    }

    public TBoard(Integer color, String title, String describe, Integer userId) {
        this.color = color;
        this.title = title;
        this.describe = describe;
        this.userId = userId;
    }

    public TBoard(Integer id, Integer color, String title, String describe) {
        this.id = id;
        this.color = color;
        this.title = title;
        this.describe = describe;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
