package com.felixcjy.hmtaskboard.pojo;

import ohos.data.orm.OrmObject;
import ohos.data.orm.annotation.Entity;
import ohos.data.orm.annotation.PrimaryKey;

/**
 * 数据库表：isLogin，用于是否登录。
 * @author 蔡济阳（FelixCJY / FelixCai）
 * @date 2022/04/08 12:20
 **/

@Entity(tableName = "isLogin")
public class IsLogin extends OrmObject {

    @PrimaryKey(autoGenerate = true)
    Integer id;
    Integer state;

    public IsLogin() {
    }

    public IsLogin(Integer id, Integer state) {
        this.id = id;
        this.state = state;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
