package com.felixcjy.hmtaskboard.pojo;

import ohos.data.orm.OrmObject;
import ohos.data.orm.annotation.Entity;
import ohos.data.orm.annotation.PrimaryKey;

/**
 * 数据库表：TUser，用于记录用户信息。
 *
 * @author 蔡济阳（FelixCJY / FelixCai）
 * @date 2022/04/08 15:28
 **/

@Entity(tableName = "tUser")
public class TUser extends OrmObject {

    @PrimaryKey(autoGenerate = true)
    Integer id;
    String phone;
    String head;
    String nickName;
    String sign;
    String email;
    Integer state;
    String password;

    public TUser() {
    }

    /**
     * @param phone    电话
     * @param head     头像路径
     * @param nickName 昵称
     * @param sign     签名
     * @param email    邮件
     * @param state    状态
     * @param password 密码
     */
    public TUser(String phone, String head, String nickName, String sign, String email, Integer state, String password) {
        this.phone = phone;
        this.head = head;
        this.nickName = nickName;
        this.sign = sign;
        this.email = email;
        this.state = state;
        this.password = password;
    }

    public TUser(Integer id, String phone, String head, String nickName, String sign, String email, Integer state, String password) {
        this.id = id;
        this.phone = phone;
        this.head = head;
        this.nickName = nickName;
        this.sign = sign;
        this.email = email;
        this.state = state;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
