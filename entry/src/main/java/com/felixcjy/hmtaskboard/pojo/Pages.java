package com.felixcjy.hmtaskboard.pojo;


import ohos.data.orm.OrmObject;
import ohos.data.orm.annotation.Entity;
import ohos.data.orm.annotation.PrimaryKey;

@Entity(tableName = "pages")
public class Pages extends OrmObject {

    @PrimaryKey(autoGenerate = true)
    Integer id;
    String title;
    Integer tBoardId;

    public Pages() {
    }

    public Pages(String title, Integer tBoardId) {
        this.title = title;
        this.tBoardId = tBoardId;
    }

    public Pages(Integer id, String title, Integer tBoardId) {
        this.id = id;
        this.title = title;
        this.tBoardId = tBoardId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer gettBoardId() {
        return tBoardId;
    }

    public void settBoardId(Integer tBoardId) {
        this.tBoardId = tBoardId;
    }
}
