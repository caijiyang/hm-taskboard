package com.felixcjy.hmtaskboard.provider;

import com.felixcjy.hmtaskboard.ResourceTable;
import com.felixcjy.hmtaskboard.pojo.TBoard;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.List;

public class TBoardProvider extends BaseItemProvider {

    private static final HiLogLabel hilog_label = new HiLogLabel(HiLog.LOG_APP, 0, "hmtb_TBoardProvider");
    private static final String hilog_format = "%{public}s";

    private final List<TBoard> list;
    private final AbilitySlice slice;

    public TBoardProvider(List<TBoard> list, AbilitySlice slice) {
        this.list = list;
        this.slice = slice;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        if (list != null && position >= 0 && position < list.size()) {
            return list.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        final Component cpt;
        if (component == null) {
            cpt = LayoutScatter.getInstance(slice).parse(ResourceTable.Layout_home_item_taskboard, null, false);
        } else {
            cpt = component;
        }

        ShapeElement element = new ShapeElement();
        TBoard tBoard = list.get(i);
        Component color = cpt.findComponentById(ResourceTable.Id_id_item_tb_color);
        Text title = cpt.findComponentById(ResourceTable.Id_id_item_tb_title);
        Text describe = cpt.findComponentById(ResourceTable.Id_id_item_tb_describe);

        element.setRgbColor(getRgbColor(tBoard.getColor()));
        color.setBackground(element);
        String tb_title = tBoard.getTitle();
        if (tb_title.equals("")) {
            title.setText("（无标题）");
            title.setTextColor(new Color(0xFF8C8888));
        } else {
            title.setText(tBoard.getTitle());
            title.setTextColor(new Color(0xFF000000));
        }
        String tb_describe = tBoard.getDescribe();
        if (tb_describe.equals("")) {
            describe.setText("暂无描述");
        } else {
            describe.setText(tBoard.getDescribe());
        }
        return cpt;
    }

    private RgbColor getRgbColor(Integer color) {
        if (color == 0) {
            return new RgbColor(207,197,226);
        } else if (color == 1) {
            return new RgbColor(255,184,179);
        } else if (color == 2) {
            return new RgbColor(181,204,141);
        } else if (color == 3) {
            return new RgbColor(166,186,221);
        }
        return new RgbColor(241,215,159);
    }
}
