package com.felixcjy.hmtaskboard.provider;

import com.felixcjy.hmtaskboard.ResourceTable;
import com.felixcjy.hmtaskboard.pojo.Card;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.util.List;

public class CardProvider extends BaseItemProvider {

    private static final HiLogLabel HiLog_Label = new HiLogLabel(HiLog.LOG_APP, 0, "hmtb_CardProvider");
    private static final String HiLog_Format = "%{public}s";

    private List<Card> list;
    private AbilitySlice abilitySlice;

    public CardProvider(List<Card> list, AbilitySlice abilitySlice) {
        this.list = list;
        this.abilitySlice = abilitySlice;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        if (list != null && position >= 0 && position < list.size()) {
            return list.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        final Component cpt;
        if (component == null) {
            cpt = LayoutScatter.getInstance(abilitySlice).parse(ResourceTable.Layout_card_item,null,false);
        } else {
            cpt = component;
        }
        Text title = (Text) cpt.findComponentById(ResourceTable.Id_card_title);
        DirectionalLayout directionalLayout = cpt.findComponentById(ResourceTable.Id_card_item_common);
        DirectionalLayout directionalLayout_img = cpt.findComponentById(ResourceTable.Id_card_item_img);
        Card myCard = list.get(i);
        title.setText(myCard.getTitle());
        if (!(myCard.getDescribe() == null) && !myCard.getDescribe().equals("")) {
            Text common = new Text(null);
            common.setText(myCard.getDescribe());
            common.setTextSize(35);
            common.setTextColor(Color.GRAY);
            common.setMultipleLine(true);
            common.setMarginsTopAndBottom(3,3);
            common.setMarginsLeftAndRight(5,5);
            Image image = new Image(cpt.getContext());
            image.setPixelMap(ResourceTable.Media_card_common_show);
            image.setMarginsLeftAndRight(0,5);
            directionalLayout.removeAllComponents();
            directionalLayout.addComponent(image);
            directionalLayout.addComponent(common);
        } else {
            directionalLayout.removeAllComponents();
        }
        if (!(myCard.getImgSrc() == null) && !myCard.getImgSrc().equals("")) {
            String[] img1 = myCard.getImgSrc().split("-");
//            HiLog.info(HiLog_Label,HiLog_Format,"图片的地址："+img1[1]);
            directionalLayout_img.removeAllComponents();
            Image image = new Image(cpt.getContext());
            image.setHeight(520);
            DataAbilityHelper helper = DataAbilityHelper.creator(cpt.getContext());
            ImageSource imageSource = null;
            try {
                FileDescriptor fd = helper.openFile(Uri.parse(img1[1]), "r");
                imageSource = ImageSource.create(fd, null);
                PixelMap pixelMap = imageSource.createPixelmap(null);
                image.setPixelMap(pixelMap);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (imageSource != null) {
                    imageSource.release();
                }
            }
            directionalLayout_img.addComponent(image);
        } else {
            directionalLayout_img.removeAllComponents();
        }
        return cpt;
    }
}