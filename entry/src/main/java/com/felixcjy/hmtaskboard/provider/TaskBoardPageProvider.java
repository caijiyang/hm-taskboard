package com.felixcjy.hmtaskboard.provider;

import com.felixcjy.hmtaskboard.ResourceTable;
import com.felixcjy.hmtaskboard.pojo.Pages;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;
import java.util.List;

public class TaskBoardPageProvider extends PageSliderProvider {

    private static final HiLogLabel HiLog_Label = new HiLogLabel(HiLog.LOG_APP, 0, "hmtb_TaskBoardPageProvider");
    private static final String HiLog_Format = "%{public}s";


    // 数据实体类
    public static class TbPage{

        Pages pages;
        ListContainer listContainer;

        public TbPage() {
        }

        public TbPage(Pages pages, ListContainer listContainer) {
            this.pages = pages;
            this.listContainer = listContainer;
        }

        public Pages getPages() {
            return pages;
        }

        public void setPages(Pages pages) {
            this.pages = pages;
        }

        public ListContainer getListContainer() {
            return listContainer;
        }

        public void setListContainer(ListContainer listContainer) {
            this.listContainer = listContainer;
        }
    }

    // 数据源
    List<TbPage> listPageData;
    Context context;
    String tBoardId;

    public List<Button> listButton = new ArrayList<Button>();
    public List<Image> listImg = new ArrayList<Image>();

    public TaskBoardPageProvider() {
    }

    public TaskBoardPageProvider(List<TbPage> list, Context context,String tBoardId) {
        this.listPageData = list;
        this.context = context;
        this.tBoardId = tBoardId;
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        // 获取 页面对象，存储的是已经包装好的 listContainer
        final TbPage tbPage = listPageData.get(i);
        // 获取 完整的 listContainer
        ListContainer pageList = tbPage.listContainer;
        // 设置 标题
        Text title = new Text(context);
        title.setText(tbPage.pages.getTitle());
        DirectionalLayout directionalLayout = new DirectionalLayout(context);
        directionalLayout.setOrientation(DirectionalLayout.HORIZONTAL);
        // 设置 按钮
        Button addButton;
        addButton = new Button(context);
        addButton.setText("添加卡片");

        // 设置 listContainer 的位置属性
        DirectionalLayoutManager directionalLayoutManager = new DirectionalLayoutManager();
        directionalLayoutManager.setOrientation(Component.VERTICAL);
        pageList.setLayoutManager(directionalLayoutManager);
        pageList.setLayoutConfig(
                new StackLayout.LayoutConfig(
                        ComponentContainer.LayoutConfig.MATCH_PARENT,
                        ComponentContainer.LayoutConfig.MATCH_CONTENT
                )
        );
        // 开启回弹效果
        pageList.setReboundEffect(true);
        // 位置属性
        directionalLayout.setLayoutConfig(
                new StackLayout.LayoutConfig(
                        ComponentContainer.LayoutConfig.MATCH_PARENT,
                        ComponentContainer.LayoutConfig.MATCH_CONTENT
                )
        );
        directionalLayout.setAlignment(LayoutAlignment.VERTICAL_CENTER);
        directionalLayout.setMarginsLeftAndRight(24, 24);
        directionalLayout.setMarginsTopAndBottom(5, 15);
        title.setTextColor(Color.BLACK);
        title.setTextSize(50);
        title.setMarginsLeftAndRight(10,10);
        title.setWidth(1000);
        Image change_page = new Image(context);
        change_page.setPixelMap(ResourceTable.Media_down);
        listImg.add(change_page);
        directionalLayout.addComponent(change_page);
        directionalLayout.addComponent(title);

        // 设置 按钮 属性
        ShapeElement addCardButtonShape = new ShapeElement();
        addCardButtonShape.setShape(ShapeElement.RECTANGLE); // 设置为长方形
        addCardButtonShape.setCornerRadius(20);
        addCardButtonShape.setRgbColor(new RgbColor(156,180,222));
        addButton.setBackground(addCardButtonShape);

        addButton.setTextSize(50);
        addButton.setTextAlignment((TextAlignment.CENTER));
        addButton.setLayoutConfig(
                new StackLayout.LayoutConfig(
                        ComponentContainer.LayoutConfig.MATCH_PARENT,
                        ComponentContainer.LayoutConfig.MATCH_CONTENT
                )
        );
        addButton.setPadding(30,10,30,10);
        addButton.setMarginsLeftAndRight(10,10);
        addButton.setMarginsTopAndBottom(10,10);


        // 设置 PageSlider 的布局
        LayoutManager layoutManager = new DirectionalLayoutManager();
        layoutManager.setOrientation(Component.VERTICAL);
        componentContainer.setLayoutManager(layoutManager);

        listButton.add(addButton);
        // 添加组件到容器中。添加按钮放到标题下面，防止出现按钮被挤出去的现象。
        componentContainer.addComponent(directionalLayout);
        componentContainer.addComponent(addButton);
        componentContainer.addComponent(pageList);
        return null;
    }

    @Override
    public int getCount() {
        return listPageData.size();
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        // 直接删除所有。
        componentContainer.removeAllComponents();
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return false;
    }


}
