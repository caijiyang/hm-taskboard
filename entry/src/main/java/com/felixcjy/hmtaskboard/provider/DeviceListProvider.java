package com.felixcjy.hmtaskboard.provider;

import com.felixcjy.hmtaskboard.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;
import ohos.distributedschedule.interwork.DeviceInfo;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.List;

/**
 * @author 蔡济阳（FelixCJY / FelixCai）
 * @date 2022/05/02 14:42
 **/

public class DeviceListProvider extends BaseItemProvider {

    private static final HiLogLabel HiLog_Label = new HiLogLabel(HiLog.LOG_APP, 0, "hmtb_DeviceListProvider");
    private static final String HiLog_Format = "%{public}s";

    // 设备名称集合
    List<DeviceInfo> deviceInfoList;
    // 当前页面
    AbilitySlice abilitySlice;

    public DeviceListProvider(List<DeviceInfo> deviceInfoList, AbilitySlice abilitySlice) {
        this.deviceInfoList = deviceInfoList;
        this.abilitySlice = abilitySlice;
    }

    @Override
    public int getCount() {
        return deviceInfoList.size();
    }

    @Override
    public Object getItem(int position) {
        if (deviceInfoList != null && position >= 0 && position < deviceInfoList.size()) {
            return deviceInfoList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        final Component cpt;
        if (component == null) {
            cpt = LayoutScatter.getInstance(abilitySlice).parse(ResourceTable.Layout_share_device_item,null,false);
        } else {
            cpt = component;
        }
        Text text = cpt.findComponentById(ResourceTable.Id_share_device_name);
        text.setText(deviceInfoList.get(i).getDeviceName());
        return cpt;
    }
}
