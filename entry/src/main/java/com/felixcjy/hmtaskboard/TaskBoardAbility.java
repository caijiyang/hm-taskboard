package com.felixcjy.hmtaskboard;

import com.felixcjy.hmtaskboard.slice.TaskBoardAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class TaskBoardAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(TaskBoardAbilitySlice.class.getName());
    }
}
