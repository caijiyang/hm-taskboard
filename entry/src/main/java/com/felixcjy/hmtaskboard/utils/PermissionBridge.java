package com.felixcjy.hmtaskboard.utils;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

public class PermissionBridge {
    public static final int EVENT_PERMISSION_GRANTED = 0x0000023;
    public static final int EVENT_PERMISSION_DENIED = 0x0000024;
    private static OnPermissionStateListener onPermissionStateListener;

    private static EventHandler handler = new EventHandler(EventRunner.current()) {
        @Override
        protected void processEvent(InnerEvent event) {
            switch (event.eventId) {
                case EVENT_PERMISSION_GRANTED:
                    onPermissionStateListener.onPermissionGranted();
                    break;
                case EVENT_PERMISSION_DENIED:
                    onPermissionStateListener.onPermissionDenied();
                    break;
                default:
                    break;
            }
        }
    };
    public void setOnPermissionStateListener(OnPermissionStateListener permissionStateListener) {
        onPermissionStateListener = permissionStateListener;
    }
    public interface OnPermissionStateListener {
        void onPermissionGranted();
        void onPermissionDenied();
    }
    public static EventHandler getHandler() {
        return handler;
    }
}
