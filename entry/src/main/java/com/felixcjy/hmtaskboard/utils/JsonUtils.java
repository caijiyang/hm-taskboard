package com.felixcjy.hmtaskboard.utils;

import ohos.aafwk.ability.AbilitySlice;
import ohos.global.resource.Resource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class JsonUtils {

    /**
     * 读取 Json 文件
     * @param path 文件路径
     * @param slice 页面
     * @return json 内容
     */
    public static String getJsonFromPath(String path, AbilitySlice slice){
        // 利用资源对象获取文件数据
        try{
            Resource resource = slice.getResourceManager().getRawFileEntry(path).openRawFile();
            // 获取资源大小
            int size = resource.available();
            // 内存声明一块区域为大小为size
            byte[] buffer = new byte[size];
            // 若获取到了数据
            if(resource.read(buffer) != -1){
                return new String(buffer, StandardCharsets.UTF_8);
            }
        } catch (IOException e){
            e.printStackTrace();
        }
        return "";
    }


}
