package com.felixcjy.hmtaskboard.utils;

import ohos.event.notification.*;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.rpc.RemoteException;

import java.time.LocalDateTime;

/**
 * @author 蔡济阳（FelixCJY / FelixCai）
 * @date 2022/04/28 07:22
 **/

public class NotificationUtils {

    private static final HiLogLabel HiLog_Label = new HiLogLabel(HiLog.LOG_APP, 0, "hmtb_NotificationUtils");
    private static final String HiLog_Format = "%{public}s";

    public static NotificationSlot slot = new NotificationSlot("taskboard_slot_id", "taskboard_slot_name", NotificationSlot.LEVEL_HIGH);

    public static void initNotificationUtils(){
        slot.setEnableLight(true); // 呼吸灯
        slot.setEnableVibration(true); // 震动
        try {
            ReminderHelper.addNotificationSlot(slot);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    public static int createNotification(Integer year, Integer month, Integer dayOfMonth, Integer hour, Integer minute,String title,String content) {
        int reminderId = -1; // DevStudio 划线是因为该变量会被重新分配地址。
        int[] repeatMonths = {};
        int[] repeatDays = {};
        HiLog.info(HiLog_Label,HiLog_Format,"创建通知："+year+"."+month+"."+dayOfMonth+" 时间："+hour+":"+minute+":"+" 标题："+title+" 内容："+content);
        ReminderRequestCalendar reminderRequestCalendar =
                new ReminderRequestCalendar(LocalDateTime.of(year, month, dayOfMonth, hour, minute),
                        repeatMonths, repeatDays);
        reminderRequestCalendar.setTitle(title);
            reminderRequestCalendar.setContent(content);
        reminderRequestCalendar.setSlotId(slot.getId());
        reminderRequestCalendar.setActionButton("关闭", ReminderRequest.ACTION_BUTTON_TYPE_CLOSE);
        try {
            reminderId = ReminderHelper.publishReminder(reminderRequestCalendar);
        } catch (ReminderManager.AppLimitExceedsException | ReminderManager.SysLimitExceedsException | RemoteException e) {
            e.printStackTrace();
        }
        return reminderId;
    }

    public static void closeNotification(int reminderId) {
        try {
//            HiLog.info(HiLog_Label, HiLog_Format, "取消前" + ReminderHelper.getValidReminders().size()+"---取消的是："+reminderId);
            ReminderHelper.cancelReminder(reminderId);
//            HiLog.info(HiLog_Label, HiLog_Format, "取消后" + ReminderHelper.getValidReminders().size()+"---取消的是："+reminderId);
        } catch (RemoteException ex) {
//            HiLog.info(HiLog_Label, HiLog_Format, "Exception occurred during cancelNotification invocation.");
            HiLog.error(HiLog_Label, HiLog_Format, "取消通知出现异常");
        }
    }

}
