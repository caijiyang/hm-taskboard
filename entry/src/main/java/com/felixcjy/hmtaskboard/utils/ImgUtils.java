package com.felixcjy.hmtaskboard.utils;

import ohos.aafwk.ability.AbilitySlice;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;

public class ImgUtils {
    public static PixelMap getPixeMapFromPath(String imageUrl, AbilitySlice slice){
        try {
            // 从路径中读取出了图片文件的数据
            Resource resource = slice.getResourceManager().getRawFileEntry(imageUrl).openRawFile();
            // 对图片文件进行说明,我们采用的是标准文件，无需特殊说明
            ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
            // 创建图片资源对象
            ImageSource imageSource = ImageSource.create(resource,sourceOptions);
            // 需要对输出的图片进行处理，输出的为标准格式，无需特殊处理。
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            // 输出一张图片
            return imageSource.createPixelmap(decodingOptions);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
